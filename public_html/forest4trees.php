<?PHP

include "common.php" ;

function get_csr_list ( $language , $depth , $category ) {
	$x = array () ;
	return db_get_articles_in_category ( $language , $category , $depth , 0 , $x ) ;
}

$depth = get_request ( 'depth' , '9' ) ;
$category = get_request ( 'category' , "" ) ;
$language = get_request ( 'language' , "en" ) ;

print '<html><head><meta http-equiv="Content-Type" content="text/html; charset=utf-8" />' ;
//print '<script src="./lib/sorttable.js"></script>' ; // http://www.kryogenix.org/code/browser/sorttable/
print '</head><body>' ;
print get_common_header ( "forest4trees.php" , "Forest 4 the Trees" ) ;

print "<h1>Forest 4 the Trees</h1>
Scans a category tree in a language and all its language-linked counterparts, then shows articles in other languages that have counterparts in the original language which are not part of the category tree." ;

print "<form method='get'>
<table border='1'>
<tr><th>Language</th><td><input name='language' value='$language' size='5' />.wikipedia</td></tr>
<tr><th>Category root</th><td><input name='category' value='$category' size='50' /></td></tr>
<tr><th>Depth</th><td><input name='depth' value='$depth' size='5' /></td></tr>
<tr><th></th><td><input type='submit' name='doit' value='Do it!' /></td></tr>
</table>
</form>" ;


if ( isset ( $_REQUEST['doit'] ) ) {
	$orig = get_csr_list ( $language , $depth , $category ) ;
//	print implode ( ", " , $orig ) . "<hr/>" ;
	$wq = new WikiQuery ( $language , 'wikipedia' ) ;
	$data = $wq->get_language_links ( 'Category:' . $category ) ;
	foreach ( $data AS $nl => $nc ) {
		$nc = array_pop ( explode ( ':' , $nc , 2 ) ) ;
//		print "$nl : $nc<br/>" ;
		$d2 = get_csr_list ( $nl , $depth , $nc ) ;
		foreach ( $d2 AS $ntitle ) {
			$ll = db_get_language_links ( $ntitle , $nl , 'wikipedia' ) ;
			if ( !isset ( $ll[$language] ) ) continue ; // No language link to original language
			$ll = str_replace ( ' ' , '_' , $ll[$language] ) ;
			if ( isset ( $orig[$ll] ) ) continue ; // Already in tree
			print "$nl:<a target='_blank' href='http://$nl.wikipedia.org/wiki/" . urlencode ( $ntitle ) . "'>$ntitle</a> &rarr; $language:<a target='_blank' href='http://$language.wikipedia.org/wiki/".urlencode($ll)."'>$ll</a><br/>" ;
		}
	}
}

print "</body></html>" ;

?>
