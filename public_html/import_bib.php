<?PHP

include "common.php" ;

class XMLtree {
	public $language , $project , $file ;
	public $used_templates ;
	public $meta_tl , $meta_cat ;
	public $namespaces ;
	public $errors ;
	public $namespace_prefix ;
	public $seen_good_template ;
	public $seen_bad_template ;
	public $seen_bad_category ;
	
	function XMLtree ( $l , $p , $f ) {
		$this->language = $l ;
		$this->project = $p ;
		$this->file = $f ;
		$this->meta_tl = Array () ;
		$this->meta_cat = Array () ;
		$this->errors = Array () ;
		$this->namespaces = Array () ;
		$this->namespace_prefix = "" ;
		$this->seen_good_template = false ;
		$this->seen_bad_template = false ;
		$this->seen_bad_category = false ;
	}
	
	function get_original_wikitext () {
		$url = "http://{$this->language}.{$this->project}.org/w/index.php?action=raw&title=" . urlencode ( $this->namespace_prefix . $this->file ) ;
		return file_get_contents ( $url ) ;
	}

	function get_xml_url () {
		$url = "http://toolserver.org/~magnus/wiki2xml/w2x.php?doit=1&whatsthis=articlelist&site={$this->language}.{$this->project}.org/w&output_format=xml&use_templates=none&text=" . urlencode ( $this->namespace_prefix . $this->file ) ;
		return $url ;
	}
	
	function get_xml ( $url = '' ) {
		if ( $url == '' ) $url = $this->get_xml_url() ;
		
		$xml = file_get_contents ( $url ) ;

		$cnt = 0 ;
		$t = '' ;
		$xml_array = explode ( '<' , $xml ) ;
		$ret = $this->parse_xml ( $xml_array , $cnt , $t , false ) ;
		while ( count ( $ret ) == 1 && $ret[0]['?'] == 'x' && ( $ret[0]['n'] == 'ARTICLES' || $ret[0]['n'] == 'ARTICLE' ) ) $ret = $ret[0]['s'] ;

		return $ret ;
	}
	
	function parse_xml_attributes ( $attrs ) {
		$ret = Array () ;
		$attrs = trim ( $attrs ) ;
		if ( $attrs == '' ) return $ret ;
		
		$attrs .= ' ' ;
		$parts = Array () ;
		$q = '' ;
		$last_was_backslash = false ;
		$p = 0 ;
		while ( $attrs != '' ) {
			$n = substr ( $attrs , $p , 1 ) ;
			
			if ( $n == ' ' && $q == '' ) {
				$parts[] = substr ( $attrs , 0 , $p ) ;
				$attrs = trim ( substr ( $attrs , $p ) ) ;
				if ( $attrs != '' ) $attrs .= ' ' ;
				$p = 0 ;
				continue ;
			}
			
			$p++ ;
			if ( ( $n == '"' || $n == "'" ) && !$last_was_backslash ) {
				if ( $q == '' ) $q = $n ;
				elseif ( $q == $n ) $q = '' ;
				continue ;
			}
			
			if ( $n == '\\' ) {
				if ( $last_was_backslash ) $last_was_backslash = false ;
				else $last_was_backslash = true ;
				continue ;
			}
		}
		
		foreach ( $parts AS $p ) {
			$n = explode ( '=' , $p , 2 ) ;
			$key = trim ( strtolower ( $n[0] ) ) ;
			if ( count ( $n ) == 1 ) {
				$ret[$key] = '\\' ; # Impossible value to indicate missing value
			} else {
				$value = trim ( $n[1] ) ;
				$ret[$key] = $value ;
			}
		}
		
		return $ret ;
	}
	
	function parse_xml ( $xml_array , &$count , &$outer_text , $in_template ) {
		$ret = Array () ;
		if ( $outer_text != '' ) {
			$ret[] = Array (
				'?' => 't' ,
				't' => $outer_text
			) ;
			$outer_text = '' ;
		}

		while ( $count < count ( $xml_array ) ) {
			$x = $xml_array[$count++] ;
			$y = explode ( '>' , $x , 2 ) ;
			if ( count ( $y ) == 0 ) $y[] = '' ;
			$tag = trim ( $y[0] ) ;
			$text = $y[1] ;

			if ( $tag == '' && $text == '' ) continue ; # Nothing for you to see here, please move along
			if ( substr ( $tag , 0 , 4 ) == '?xml' ) continue ; # Don't care
			
			$is_closing = false ;
			if ( substr ( $tag , 0 , 1 ) == '/' ) {
				$is_closing = true ;
				$tag = trim ( substr ( $tag , 1 ) ) ;
			}
			
			if ( $is_closing ) { # My work here is done
				$outer_text = $text ;
				break ;
			}
			
			$is_self_closing = false ;
			if ( substr ( $tag , -1 , 1 ) == '/' ) {
				$is_self_closing = true ;
				$tag = trim ( substr ( $tag , 0 , -1 ) ) ;
			}
			
			$y = explode ( ' ' , $tag , 2 ) ;
			if ( count ( $y ) == 1 ) $y[] = '' ;
			$tag = strtoupper ( $y[0] ) ;
			$attrs = $y[1] ;
			
			if ( $tag != '' ) {
				$n = Array (
					'?' => 'x' ,
					'n' => $tag ,
					'a' => $this->parse_xml_attributes ( $attrs ) ,
					'sc' => $is_self_closing
				) ;
				if ( !$is_closing && !$is_self_closing ) {
					$n['s'] = $this->parse_xml ( $xml_array , $count , $text , $tag == 'TEMPLATE' ) ;
				}
				$ret[] = $n ;
			}

			if ( $text != '' ) {
				$ret[] = Array (
					'?' => 't' ,
					't' => $text
				) ;
			}

		}
		
		$ret2 = Array () ;
		foreach ( $ret AS $r ) {
			if ( $r['?'] == 'x' && $r['sc'] && $r['n'] == 'SPACE' ) {
				$r = Array (
					'?' => 't' ,
					't' => ' '
				) ;
			}
			
			$cr = count ( $ret2 ) ;
			if ( $cr > 0 && $ret2[$cr-1]['?'] == 't' && $r['?'] == 't' ) {
				$ret2[$cr-1]['t'] .= $r['t'] ;
			} else $ret2[] = $r ;
		}
		
		// Expand template arguments
		if ( $in_template ) {
			foreach ( $ret2 AS $k => $v ) {
				if ( $v['?'] != 'x' ) continue ;
				if ( $v['n'] == 'TARGET' ) {
					$tn = ucfirst ( $v['s'][0]['t'] ) ;
					$v['s'][0]['t'] = $tn ; // Alters tree to enforce ufcirst template names
					$this->used_templates[$tn] = $tn ;
				}
				if ( $v['n'] != 'ARG' ) continue ;
				if ( !isset ( $v['s'] ) ) continue ;
				if ( count ( $v['s'] ) != 1 ) continue ;
				$n = $v['s'][0] ;
				if ( $n['?'] != 't' ) continue ;
				
				$text = trim ( $n['t'] ) ;
				if ( $text == '' ) continue ; // No need to parse this...
				
				$n = $this->fix_template_parameter ( $text ) ;
				if ( count ( $n ) == 1 && $n[0]['?'] == 'x' && $n[0]['n'] == 'PARAGRAPH' && isset ( $n[0]['s'] ) ) $n = $n[0]['s'] ;
				$ret2[$k]['s'] = $n ;
			}
		}
		
		return $ret2 ;
	}

	function is_information_template ( $name ) {
		return false ;
	}
	
	function fix_template_parameter ( $s ) {
		$s = urlencode ( $s ) ;
		$url = "http://toolserver.org/~magnus/wiki2xml/w2x.php?doit=1&whatsthis=wikitext&site={$this->language}.{$this->project}.org/w&output_format=xml&use_templates=none&text=" . $s ;
		return $this->get_xml ( $url ) ;
	}
	
}


class XML2wiki {
	public $prelist ;
	public $pretemplate ;
	public $remove_empty_fields ;
	
	function XML2wiki () {
		$this->prelist = '' ;
		$this->pretemplate = '' ;
		$this->remove_empty_fields = false ;
	}
	
	function getsub ( $xml ) {
		if ( !isset ( $xml['s'] ) ) return '' ;
		return $this->convert ( $xml['s'] ) ;
	}
	
	function strip_attr_quotes ( $attr ) {
		if ( substr ( $attr , 0 , 1 ) != '"' && substr ( $attr , 0 , 1 ) != "'" ) return $attr ;
		if ( substr ( $attr , -1 , 1 ) != '"' && substr ( $attr , -1 , 1 ) != "'" ) return $attr ;
		if ( substr ( $attr , 0 , 1 ) != substr ( $attr , -1 , 1 ) ) return $attr ;
		return substr ( $attr , 1 , -1 ) ;
	}
	
	function get_link_or_template ( $xml , $is_template , $parent ) {
		if ( $is_template ) $pt = substr ( $this->pretemplate , 0 , -1 ) ;
		$ret = Array() ;
		$trail = '' ;
		$ext_url = '' ;
		$ext_name = '' ;
		$is_external_link = false ;
		if ( !$is_template && isset ( $parent ) && isset ( $parent['a']['type'] ) && $this->strip_attr_quotes($parent['a']['type']) == 'external' ) {
			$is_external_link = 1 ;
			$ext_url = $this->strip_attr_quotes ( $parent['a']['href'] ) ;
		}
		foreach ( $xml AS $x ) {
			$sub = $this->getsub ( $x ) ;
			if ( $x['n'] == 'TARGET' ) {
				if ( $is_template ) $ret[] = "{{" . $sub ;
				else $ret[] = "[[$sub" ;
			} elseif ( $x['n'] == 'ARG' || $x['n'] == 'PART' ) {
				$sub = trim ( $sub ) ;
				if ( $is_template ) {
					$r = "|" ;
					if ( isset ( $x['a']['name'] ) ) {
						$keyname = $this->strip_attr_quotes ( $x['a']['name'] ) ;
						if ( !is_numeric ( $keyname ) ) $r .= $keyname . ' = ' ;
					}
					$r .= "$sub" ;
					if ( $sub != '' or !$this->remove_empty_fields ) $ret[] = $r ;
				} else {
					$ret[] = "|$sub" ;
				}
			} elseif ( $x['n'] == 'TRAIL' ) {
				$trail = $sub ;
			} elseif ( $is_external_link ) {
			} else print "OH-OH #1 : !" . $x['n'] . "!<br/>" ;
		}
		
		if ( $is_external_link ) {
			$ext_name = $this->getsub ( $parent ) ;
			if ( $ext_name != '' ) $ret[] = "[$ext_url $ext_name]" ;
			else $ret[] = $ext_url ;
		}
		
		if ( $is_template ) {
			$r = implode ( '' , $ret ) ;
			if ( strlen ( $r ) < 80 ) $ret = implode ( '' , $ret ) ;
			else $ret = implode ( "\n$pt" , $ret ) . "\n$pt" ;
		} else $ret = implode ( '' , $ret ) ;
		
		if ( $is_template ) $ret .= "}}" ;
		else if ( !$is_external_link ) $ret .= "]]" ;
		return $ret . $trail ;
	}
	
	function convert ( $xml , $depth = 0 ) {
		$ret = '' ;
		$first_tablerow = true ;
		$first_tablecell = true ;
		foreach ( $xml AS $x ) {
			if ( $x['?'] == 't' ) { // Plain text
				$ret .= $x['t'] ;
				continue ;
			}
			
			if ( $x['?'] != 'x' ) {
				print "BAD INTERNAL TYPE : '" . $x['?'] . "'!<br/>" ;
				continue ;
			}
			
			$tag = $x['n'] ;
			if ( $tag == 'PARAGRAPH' ) {
				$ret .= $this->getsub ( $x ) . "\n\n" ;
			} else if ( $tag == 'LINK' ) {
				$ret .= $this->get_link_or_template ( $x['s'] , false , $x ) ;
			} else if ( $tag == 'TEMPLATE' ) {
				$this->pretemplate .= ' ' ;
				$ret .= $this->get_link_or_template ( $x['s'] , true ) ;
				$this->pretemplate = substr ( $this->pretemplate , 0 , -1 ) ;
			} else if ( $tag == 'BOLD' ) {
				$ret .= "'''" . $this->getsub ( $x ) . "'''" ;
			} else if ( $tag == 'ITALICS' ) {
				$ret .= "''" . $this->getsub ( $x ) . "''" ;
			} else if ( $tag == 'PREBLOCK' ) {
				$ret .= $this->getsub ( $x ) . "\n" ;
			} else if ( $tag == 'PRELINE' ) {
				$ret .= " " . $this->getsub ( $x ) . "\n" ;
			} else if ( $tag == 'HEADING' ) {
				if ( isset ( $x['a']['level'] ) ) $level = $this->strip_attr_quotes ( $x['a']['level'] ) ;
				else $level = 1 ; // Paranoia fallback
				$eq = str_repeat ( '=' , $level ) ;
				$ret .= "\n$eq " . $this->getsub ( $x ) . " $eq\n" ;
			} else if ( substr ( $tag , 0 , 6 ) == 'XHTML:' ) {
				$tag = strtolower ( substr ( $tag , 6 ) ) ;
				$ret .= "<$tag" ;
				$ret .= $this->collapse_attributes ( $x ) ;
				if ( !$x['sc'] ) {
					$ret .= '>' ;
					$ret .= $this->getsub ( $x ) ;
					$ret .= "</$tag>" ;
				} else $ret .= "/>" ;
			} else if ( $tag == 'LIST' ) {
				$type = $this->strip_attr_quotes ( $x['a']['type'] ) ;
				$ret .= "\n" ;
				if ( $type == 'bullet' ) $this->prelist .= '*' ;
				else if ( $type == 'ident' ) $this->prelist .= ':' ;
				else if ( $type == 'numbered' ) $this->prelist .= '#' ;
				else if ( $type == 'def' ) $this->prelist .= ';' ;
				$ret .= $this->getsub ( $x ) . "\n" ;
				$this->prelist = substr ( $this->prelist , 0 , -1 ) ;
				if ( $this->prelist == '' ) $ret .= "\n" ;
			} else if ( $tag == 'LISTITEM' ) {
				$ret .= "\n" . $this->prelist . ' ' . $this->getsub ( $x ) ;
			} else if ( $tag == 'DEFKEY' ) {
				$ret .= $this->getsub ( $x ) ;
			} else if ( $tag == 'DEFVAL' ) {
				$ret .= " : " . $this->getsub ( $x ) ;
			} else if ( $tag == 'TABLE' ) {
				$ret .= "\n{|" . $this->collapse_attributes ( $x ) . "\n" ;
				$ret .= $this->getsub ( $x ) ;
				$ret .= "\n|}\n\n" ;
			} else if ( $tag == 'TABLEROW' ) {
				if ( !$first_tablerow ) $ret .= "\n|-" . $this->collapse_attributes ( $x ) ;
				$ret .= "\n" ;
				$ret .= $this->getsub ( $x ) ;
				$first_tablerow = false ;
			} else if ( $tag == 'TABLECAPTION' ) {
				$ret .= "\n|+ " . $this->collapse_attributes ( $x ) ;
				$ret .= $this->getsub ( $x ) ;
			} else if ( $tag == 'TABLEHEAD' or $tag == 'TABLECELL' ) {
				if ( $tag == 'TABLEHEAD' ) $sep = '!' ;
				else if ( $tag == 'TABLECELL' ) $sep = '|' ;
				$r = $this->getsub ( $x ) ;
				$a = $this->collapse_attributes ( $x ) ;
				if ( strlen ( $r ) < 20 ) {
					if ( !$first_tablecell ) $ret .= $sep ;
					$ret .= $sep ;
				} else {
					$ret .= "\n$sep" ;
				}
				if ( trim ( $a ) != '' ) $ret .= "$a$sep" ;
				$ret .= $r ;
				
				if ( $tag == 'TABLEHEAD' ) {
					$first_tablecell = true ;
					$ret .= "\n" ;
				} else $first_tablecell = false ;
			} else if ( $tag == 'EXTENSION' ) {
				$ext = $this->strip_attr_quotes ( $x['a']['extension_name'] ) ;
				unset ( $x['a']['extension_name'] ) ;
				$ret .= "<$ext" . $this->collapse_attributes ( $x ) ;
				$ret .= '>' . $this->getsub ( $x ) . "</$ext>" ;
			} else if ( $tag == 'MAGIC_VARIABLE' ) {
				$ret .= '__' . $this->getsub ( $x ) . "__" ;
			} else {
				print "<b>UNKNOWN TAG : '$tag'! Please note this error, the project, language, and filename to <a href='http://en.wikipedia.org/wiki/User:Magnus_Manske'>the author</a> so it can be fixed!</b><br/>" ;
				$ret .= '{{CommonsHelper2 malfunction}}' ;
				$ret .= $this->getsub ( $x ) ;
			}
			
		}
		if ( $depth == 0 ) $this->post_process_wikitext ( $ret ) ;
		return $ret ;
	}
	
	function collapse_attributes ( $x ) {
		if ( !isset ( $x['a'] ) ) return '' ;
		$ret = '' ;
		foreach ( $x['a'] AS $k => $v ) {
			if ( $v == '\\' ) $ret .= " $k" ;
			else $ret .= " $k=$v" ;
		}
		return $ret ;
	}
	
	function post_process_wikitext ( &$wiki ) {
		$wiki = trim ( $wiki ) ;
		$wiki = str_replace ( '}}{{' , "}}\n{{" , $wiki ) ;
		$wiki = str_replace ( '}}[[' , "}}\n[[" , $wiki ) ;
		$wiki = str_replace ( "=\n\n*" , "=\n*" , $wiki ) ;
		$wiki = str_replace ( '<br></br>' , '<br/>' , $wiki ) ;
		$wiki = str_replace ( '<references></references>' , '<references/>' , $wiki ) ;
		
		$count = 0 ;
		do {
			$wiki = str_replace ( "\n\n\n" , "\n\n" , $wiki , $count ) ;
		} while ( $count > 0 ) ;
	}
	
}





##########

$items = array () ;
$keys = array () ;

function check_Literatur ( $xml ) {
	global $items , $keys ;
	$name = $xml[0]['s'][0]['t'] ;
	if ( !isset ( $name ) ) return false ;
	if ( ucfirst ( $name ) != 'Literatur' ) return false ;
	
//	print "<h2>$name</h2>" ;
	$i = array () ;
	for ( $p = 1 ; $p < count ( $xml ) ; $p++ ) {
		$y = $xml[$p] ;
		if ( $y['n'] != 'ARG' ) continue ;

		$key = $y['a']['name'] ;
		if ( !isset ( $key ) ) continue ;


		$x2w = new XML2wiki () ;
		$key = $x2w->strip_attr_quotes ( $key ) ;
		$w = trim ( $x2w->convert ( $y['s'] ) ) ;
		
		if ( $w == '' ) continue ;
		
		$i[$key] = $w ;
		$keys[$key] = 1 ;
		
//		print "!$key:$w!<br/>" ;
//		print "<pre>" ; print_r ( $y ) ; print "</pre>" ; 
	}
	
	$items[] = $i ;
	
//	print "<pre>" ; print_r ( $name ) ; print "</pre>" ; 
	return true ;
}

function check_Cite_book ( $xml ) {
	global $items , $keys ;
	$name = $xml[0]['s'][0]['t'] ;
	if ( !isset ( $name ) ) return false ;
	$name = str_replace ( '_' , ' ' , $name ) ;
	if ( ucfirst ( $name ) != 'Cite book' ) return false ;
	
	$trans = array (
		'author' => 'Autor' ,
		'title' => 'Titel' ,
		'url' => 'Online' ,
		'accessdate' => 'Zugriff' ,
		'date' => 'Jahr' ,
		'isbn' => 'ISBN' ,
		'doi' => 'DOI'
	) ;
	
//	print "<h2>$name</h2>" ;
	$i = array () ;
	for ( $p = 1 ; $p < count ( $xml ) ; $p++ ) {
		$y = $xml[$p] ;
		if ( $y['n'] != 'ARG' ) continue ;

		$key = $y['a']['name'] ;
		if ( !isset ( $key ) ) continue ;

		$x2w = new XML2wiki () ;
		$key = $x2w->strip_attr_quotes ( $key ) ;
		$w = trim ( $x2w->convert ( $y['s'] ) ) ;
		
		if ( !isset ( $trans[$key] ) ) continue ;
		$key = $trans[$key] ;
		
		if ( $w == '' ) continue ;
		
		$i[$key] = $w ;
		$keys[$key] = 1 ;
		
//		print "!$key:$w!<br/>" ;
//		print "<pre>" ; print_r ( $y ) ; print "</pre>" ; 
	}
	
	$items[] = $i ;
	
//	print "<pre>" ; print_r ( $name ) ; print "</pre>" ; 
	return true ;
}

function check_listitem ( $xml ) {
	global $items , $keys ;
	$italics = '' ;
	$pre = '' ;
	$post = '' ;
	$x2w = new XML2wiki () ;
	foreach ( $xml AS $x ) {
		if ( $x['?'] == 'x' && $x['n'] == 'ITALICS' ) {
			if ( $italics != '' ) return false ;
			$italics = $x2w->convert ( $x['s'] ) ;
		} else {
			if ( $x['?'] == 't' ) $wiki = $x['t'] ;
			else {
				$y = array ( 0 => $x ) ;
				$wiki = $x2w->convert ( $y ) ;
			}
			if ( $italics == '' ) $pre .= $wiki ;
			else $post .= $wiki ;
		}
	}
	if ( $post == '' ) return false ;
	
//	$isbn_pattern = "/ISBN\s(?=[-0-9xX ]{13}$)(?:[0-9]+[- ]){3}[0-9]*[xX0-9]/" ;
	$isbn_pattern = "/ISBN\s*([0-9- ]+[xX]{0,1})/" ;
	$isbn = array () ;
	if ( 1 != preg_match ( $isbn_pattern , $post , $isbn ) ) return false ; // No single ISBN
	
	$post = preg_replace ( $isbn_pattern , '' , $post ) ;
	$isbn = $isbn[0] ;
	$isbn = preg_replace ( "/^ISBN\s*/" , '' , $isbn ) ;
	
	$pre = trim ( $pre ) ;
	$italics = trim ( $italics ) ;
	$post = trim ( $post ) ;
	
	$pre = rtrim ( $pre , ':; ' ) ;
	$italics = rtrim ( $italics , ';. ' ) ;
	$post = trim ( $post , ';., ' ) ;
	
	$year = array () ;
	$pattern_year = "/(\d{3,4})$/" ;
	if ( preg_match ( $pattern_year , $post , $year ) == 1 ) {
		$year = $year[0] ;
		$post = preg_replace ( $pattern_year , '' , $post ) ;
		$post = rtrim ( $post , ';., ' ) ;
	} else $year = '' ;
	
//	print "<pre>$pre\n$italics\n$post\nJahr:$year\n$isbn</pre>" ;
	
	$i = array () ;
	$i['Autor'] = $pre ;
	$i['Titel'] = $italics ;
	if ( $year != '' ) $i['Jahr'] = $year ;
	$i['ISBN'] = $isbn ;
	if ( $post != '' ) $i['Verlag'] = $post ;
	
	foreach ( array_keys ( $i ) AS $k ) $keys[$k] = 1 ;
	$items[] = $i ;
	
	return true ;
}

function parse_xml ( &$xml , $depth = 0 ) {
	foreach ( $xml AS $x ) {
		if ( $x['?'] == 'x' ) {
//			if ( !isset ( $x['s'] ) ) continue ;
//			if ( count ( $x['s'] ) == 0 ) continue ;
			if ( $x['n'] == 'TEMPLATE' ) {
				if ( check_Literatur ( $x['s'] ) ) continue ;
				if ( check_Cite_book ( $x['s'] ) ) continue ;
			}
			if ( $x['n'] == 'LISTITEM' && $depth == 1 ) {
				if ( check_listitem ( $x['s'] ) ) continue ;
			}
//			print $x['n'] . "<br/>" ;
			parse_xml ( $x['s'] , $depth + 1 ) ;
		}
	}
}


function analysis ( $title ) {
	global $items , $keys ;
	$xt = new XMLtree ( 'de' , 'wikipedia' , $title ) ;
	$xml = $xt->get_xml () ;
	
	print "<h1>$title</h1>" ;
	
	parse_xml ( $xml ) ;
	
	
	$ids = array () ;
	foreach ( $items AS $k => $i ) {
		$isbn = $i['ISBN'] ;
		if ( !isset ( $isbn ) ) $isbn = '' ;
		$id = preg_replace ( "/[^\dxX]/" , '' , $isbn ) ;
		$items[$k]['id'] = $id ;
		
		if ( $id == '' ) continue ;
		
		if ( !isset ( $ids[$id] ) ) $ids[$id] = array () ;
		$i['num'] = $k ;
		$ids[$id][] = $i ;
	}
//	$keys['id'] = 1 ;

	$pages = array () ;
	$page2id = array () ;
	foreach ( $ids AS $id => $data ) {
		$p = "Vorlage:BibISBN/$id" ;
		$pages[] = $p ;
		$page2id[$p] = $id ;
//		$ids[$id]['exists'] = 0 ;
	}
	
	$wq = new WikiQuery ( 'de' , 'wikipedia' ) ;
	$existing = $wq->get_existing_pages ( $pages ) ;
	$ex_ids = array () ;
	foreach ( $existing AS $e ) {
		$id = $page2id[$e] ;
		$ex_ids[$id] = 1 ;
	}

	$mysql_con = db_get_con () ;
	$db = 'u_magnus_yarrow' ;

	print "<h3>Individuelle Einträge</h3><table border='1'><tr><th>#</th><th>ID</th><th>Benutzt</th><th>BibISBN</th></tr>" ;
	$cnt = 0 ;
	foreach ( $ids AS $id => $data ) {
		$cnt++ ;
		print "<tr><td>$cnt</td><th>$id</th>" ;
		print "<td>" . count ( $data ) . "&times;</td><td>" ;

		$indb = false ;
		$sql = "SELECT * FROM bibrec WHERE bibrec='$id' LIMIT 1" ;
		$res = my_mysql_db_query ( $db , $sql , $mysql_con ) ;
		while ( $o = mysql_fetch_object ( $res ) ) {
			$indb = true ;
		}

		
		if ( isset ( $ex_ids[$id] ) ) {
			print "<tt>{{BibISBN|".$id."}}</tt>" ;
		} else {
			$url = 'http://toolserver.org/~magnus/bibcomm.php?direct=1&mode=add&type=isbn' ;
			$entry = $data[0] ;
			for ( $a = 1 ; $a < count ( $data ) ; $a++ ) {
				foreach ( $data[$a] AS $k => $v ) {
					if ( $entry[$k] != '' ) continue ;
					$entry[$k] = $v ; // New value in dataset
				}
			}
			foreach ( $entry AS $k => $v ) {
				$k = 'entry_' . urlencode ( strtolower ( $k ) ) ;
				$v = urlencode ( $v ) ;
				$url .= "&$k=$v" ;
			}
			print "<a href='$url' target='_blank'>anlegen aus Artikel</a>" ;
			
			if ( $indb ) {
				$url = "http://toolserver.org/~magnus/bibcomm.php?direct=1&mode=new&type=fromdb&id=$id" ;
				print " ; <a href='$url' target='_blank'>anlegen aus Datenbank</a>" ;
			}
		}
		
		print "</td></tr>" ;
	}
	print "</table>" ;
	
	print "<h3>Gesamtübersicht</h3>" ;
	print "<table border=1>" ;
	
	print "<tr><th></th>" ;
	foreach ( $items AS $k => $i ) {
		$k++ ;
		print "<th>$k</th>" ;
	}
	print "</tr>" ;
	$kx = array_keys ( $keys ) ;
	foreach ( $kx AS $k ) {
		print "<tr><th>$k</th>" ;
		foreach ( $items AS $i ) {
			$v = $i[$k] ;
			if ( !isset ( $v ) ) $v = '' ;
			if ( $v != '' ) print "<td>$v</td>" ;
			else print "<td>&nbsp;</td>" ;
		}
		print "</tr>" ;
	}
	
	print "</table>" ;
}


###############################

#$title = 'Eroberung von Stalingrad' ;

$title = $_REQUEST['title'] ;

print '<html><head><meta http-equiv="content-type" content="application/xhtml+xml; charset=UTF-8" /></head><body>' ;
print get_common_header ( "import_bib.php" , "ImportBib" ) ;


#print "<p>UNDER CONSTRUCTION, things might not work!</p>" ;

if ( isset ( $title ) ) {
	analysis ( $title ) ;
} else {
	print "<h1>Literaturdaten-Analyse und -Import</h1>" ;
	print "<form method='get' action='import_bib.php'>" ;
	print "Titel : <input type='text' size='50' name='title' /><input type='submit' name='doit' value='Los!' />" ;
	print "</form>" ;
}

/*
print "<pre>" ;
print_r ( $xml ) ;
print "</pre>" ;
*/

print "</body></html>" ;
?>