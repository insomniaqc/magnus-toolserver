<?PHP

$hide_header = 1 ;
$hide_doctype = 1 ;


include_once ( 'common.php' ) ;

// Report all PHP errors
error_reporting(E_ALL);

$license_names = array (
	0 => 'All Rights Reserved',
	1 => 'Attribution-NonCommercial-ShareAlike License',
	2 => 'Attribution-NonCommercial License',
	3 => 'Attribution-NonCommercial-NoDerivs License',
	4 => 'Attribution License',
	5 => 'Attribution-ShareAlike License',
	6 => 'Attribution-NoDerivs License',
	999 => 'Other free license',
) ;

$license_urls = array (
	0 => '',
	1 => 'http://creativecommons.org/licenses/by-nc-sa/2.0/',
	2 => 'http://creativecommons.org/licenses/by-nc/2.0/',
	3 => 'http://creativecommons.org/licenses/by-nc-nd/2.0/',
	4 => 'http://creativecommons.org/licenses/by/2.0/',
	5 => 'http://creativecommons.org/licenses/by-sa/2.0/',
	6 => 'http://creativecommons.org/licenses/by-nd/2.0/',
	999 => 'http://commons.wikimedia.org/wiki/Category:Free_licenses',
) ;


#_________________

function wrap ( $tag , $attrs = array() , $sub = array () ) {
	$ret = array () ;
	$ret['name'] = $tag ;
	$ret['attrs'] = $attrs ;
	$ret['tags'] = $sub ;
	return array ( $ret ) ;
}

function show_xml ( &$out ) {
	$ret = '' ;
	foreach ( $out AS $v ) {
		$ret .= "<" . $v['name'] ;
		foreach ( $v['attrs'] AS $ak => $av ) {
			$ret .= " $ak=\"" ;
			$ret .= htmlspecialchars ( $av ) ;
			$ret .= "\"" ;
		}
		if ( count ( $v['tags'] ) == 0 ) {
			$ret .= " />\n" ;
		} else {
			$ret .= ">\n" ;
			$ret .= show_xml ( $v['tags'] ) ;
			$ret .= "</" . $v['name'] . ">\n" ;
		}
	}
	return $ret ;
}

function show ( &$out ) {
	global $format ;
	if ( $format == 'xmlrpc' ) {
		header('Content-type: text/xml');
		print '<?xml version="1.0" encoding="utf-8" ?>' ;
		print '<methodResponse><params><param><value><string>' ;
		print htmlspecialchars ( show_xml ( $out[0]['tags'] ) ) ;
		print '</string></value></param></params></methodResponse>' ;
	} else if ( $format == 'rest' ) {
		header('Content-type: text/xml');
		print '<?xml version="1.0" encoding="utf-8" ?>' ;
		print show_xml ( $out ) ;
	}
}

function add_size_tag ( $tw , $th , $label , $width , $height , $image , &$data ) {
	if ( $width == 0 or $height == 0 ) return ;
	if ( $tw > $width or $th > $height ) return ;
	
	# width/height = tw/th
	
	if ( $tw == $width and $th == $height ) {
		$w = $tw ;
		$h = $th ;
	} else {
		$w = $tw ;
		$h = floor ( $tw * $height / $width ) ;
		if ( $h > $th ) {
			$w = floor ( $width * $th / $height ) ;
			$h = $th ;
		}
	}
	
	$n = wrap ( 'size' , array ( 
			'label' => $label ,
			'width' => $w ,
			'height' => $h ,
			'source' => get_thumbnail_url ( 'commons' , $image , $w , 'wikimedia' ) ,
			'url' => get_wikipedia_url ( 'commons' , "Image:$image" , '' , 'wikimedia' ) ,
			'media' => 'photo' // dummy
			) ) ;
	$data[] = $n[0] ;
}

function wrap_error ( $code , $msg ) {
	return wrap ( 'rsp' , array ( 'stat' => 'fail' ) , wrap ( 'err' , array ( 'code' => $code , 'msg' => $msg ) ) ) ;
}

function remove_image_prefix ( $i ) {
	if ( substr ( strtolower ( $i ) , 0 , 6 ) == 'image:' ) return substr ( $i , 6 ) ;
	return $i ;
}

function remove_category_prefix ( $i ) {
	if ( substr ( strtolower ( $i ) , 0 , 9 ) == 'category:' ) return substr ( $i , 9 ) ;
	return $i ;
}

#___________________________

function flickr_photo_getSizes () {
	global $wq ;
	$image = remove_image_prefix ( get_request ( 'photo_id' , '' ) ) ;
	$data = $wq->get_image_data ( "Image:$image" ) ;
	if ( isset ( $data['missing'] ) ) return wrap_error ( 1 , 'Photo not found' ) ;

#		print "<pre>" ; print_r ( $data ) ; print "</pre>" ; 
	$a = array () ;
	$width = $data['image']['width'] ;
	$height = $data['image']['height'] ;
	add_size_tag ( 75 , 75 , 'Square' , $width , $height , $image , $a ) ;
	add_size_tag ( 100 , 100 , 'Thumbnail' , $width , $height , $image , $a ) ;
	add_size_tag ( 240 , 240 , 'Small' , $width , $height , $image , $a ) ;
	add_size_tag ( 500 , 500 , 'Medium' , $width , $height , $image , $a ) ;
	add_size_tag ( 1024 , 1024 , 'Large' , $width , $height , $image , $a ) ;
	add_size_tag ( $width , $height , 'Original' , $width , $height , $image , $a ) ;
	$sizes = wrap ( 'sizes' , array ( 'canblog' => 1 , 'canprint' => 1 , 'candownload' => 1 ) , $a ) ;
	return wrap ( 'rsp' , array ( 'stat' => 'ok' ) , $sizes ) ;
}

function flickr_photos_getAllContexts () {
	global $wq ;
	$image = remove_image_prefix ( get_request ( 'photo_id' , '' ) ) ;
	if ( !$wq->does_image_exist ( "Image:$image" ) ) return wrap_error ( 1 , 'Photo not found' ) ;
	$categories = $wq->get_categories  ( "Image:$image" ) ;
	$galleries = $wq->get_used_image  ( "Image:$image" ) ;
	
	$ret = array () ;
	foreach ( $categories AS $c ) $ret[] = array_shift ( wrap ( 'pool' , array ( 'id' => "Category:$c" , 'title' => $c ) ) ) ;
	foreach ( $galleries AS $g ) $ret[] = array_shift ( wrap ( 'set' , array ( 'id' => $g , 'title' => $g ) ) ) ;
	
	return wrap ( 'rsp' , array ( 'stat' => 'ok' ) , $ret ) ;
}

function flickr_photos_licenses_getInfo () {
	global $license_names , $license_urls ;
	$ret = array () ;
	foreach ( $license_names AS $num => $name ) {
		$ret[] = array_shift ( wrap ( 'license' , array ( 'id' => $num , 'name' => $name , 'url' => $license_urls[$num] ) ) ) ;
	}
	$licenses = wrap ( 'licenses' , array () , $ret ) ;
	return wrap ( 'rsp' , array ( 'stat' => 'ok' ) , $licenses ) ;
}

function flickr_groups_browse () {
	global $wq ;
	$category = remove_category_prefix ( get_request ( 'cat_id' , 'Category:CommonsRoot' ) ) ;
	$subcats = array_keys ( $wq->get_pages_in_category ( $category , 14 ) ) ;
	$ret = array () ;
	foreach ( $subcats AS $c ) {
		$c = array_pop ( explode ( ':' , $c , 2 ) ) ;
		$ret[] = array_shift ( wrap ( 'subcat' , array ( 'id' => $c , 'name' => $c ) ) ) ; # MISSING : count
	}

#	print "<pre>" ; print_r ( $subcats ) ; print "</pre>" ; 
	return wrap ( 'category' , array ( 'name' => $category , 'path' => $category , 'pathids' => $category ) , $ret ) ;
}


#___________________________

$method = get_request ( 'method' , '' ) ;
$format = get_request ( 'format' , 'rest' ) ;

$wq = new WikiQuery ( 'commons' , 'wikimedia' ) ;
$out = array () ;

if ( $method == 'flickr.photos.getSizes' ) $out = flickr_photo_getSizes () ;
else if ( $method == 'flickr.photos.getAllContexts' ) $out = flickr_photos_getAllContexts () ;

else if ( $method == 'flickr.groups.browse' ) $out = flickr_groups_browse () ;

else if ( $method == 'flickr.photos.licenses.getInfo' ) $out = flickr_photos_licenses_getInfo () ;

else {
	$out = file_get_contents ( "./cfapi.html" ) ;
	header('Content-type: text/html');
	print $out ;
	myflush() ;
	exit ;
#	$out = wrap ( 'rsp' , array ( 'stat' => 'fail' ) , wrap ( 'err' , array ( 'code' => 1 , 'msg' => '???' ) ) ) ;
}

show ( $out ) ;


?>