<?PHP

error_reporting ( E_ALL ) ;
require ( 'queryclass.php' ) ;
require ( 'phpflickr-3.0/phpFlickr.php' ) ;

$only_no_group = 1 ;
$flickr_user_name = 'temporalata' ;
$fk = get_flickr_key () ;
$flickr = new phpFlickr ( $fk ) ;

print "<html><head></head><body>" ;

$flickr_user = $flickr->people_findByUsername ( $flickr_user_name ) ;
$flickr_user_id = $flickr_user['nsid'] ;

print "Flickr user name : $flickr_user_name<br/>" ;
print "Flickr user id : $flickr_user_id<br/>" ;

$fpp = 500 ; // Files per page
$extras = 'url_sq,url_o' ;
print "Getting photo batch 1-{$fpp}…<br/>" ; myflush() ;
$files = $flickr->people_getPublicPhotos ( $flickr_user_id , 3 , $extras , $fpp , 1 ) ;
for ( $page = 2 ; $page <= $files['photos']['pages'] ; $page++ ) {
	print "Getting photo batch " . (($page-1)*$fpp+1) . "-" . ($page*$fpp) . "…<br/>" ; myflush() ;
	$f2 = $flickr->people_getPublicPhotos ( $flickr_user_id , 3 , $extras , $fpp , $page ) ;
	foreach ( $f2['photos']['photo'] AS $f => $v ) {
		$files['photos']['photo'][] = $v ;
	}
}
print count ( $files['photos']['photo'] ) . " files total.<hr/>" ;

$pics_in_groups = array () ;
$groups = $flickr->people_getPublicGroups ( $flickr_user_id ) ;
print "Groups:<ul>" ;
foreach ( $groups AS $k => $v ) {
	$gname = $v['name'] ;
	$gid = $v['nsid'] ;
	
	$pigs = $flickr->groups_pools_getPhotos ( $gid , NULL , $flickr_user_id , NULL , 500 ) ;
	foreach ( $pigs['photo'] AS $k2 => $v2 ) {
		$pid = $v2['id'] ;
		if ( !isset ( $pics_in_groups[$pid] ) ) $pics_in_groups[$pid] = array () ;
		$pics_in_groups[$pid][] = $gid ;
	}
	
	print "<li>$gname ($gid)" ;
	print "</li>" ;
	myflush();
}
print "</ul>" ;

print "<table>" ;
if ( $only_no_group == 1 ) print "<tr>" ;
$cnt = 0 ;
$total = 0 ;
foreach ( $files['photos']['photo'] AS $k => $v ) {
	$pid = $v['id'] ;
	$page = "http://www.flickr.com/photos/$flickr_user_id/$pid/" ;
	$s = '' ;
	if ( $only_no_group == 1 ) {
		if ( isset ( $pics_in_groups[$pid] ) ) continue ;
		$total++ ;
		if ( $cnt == 15 ) {
			$cnt = 0 ;
			$s .= "</tr><tr>" ;
		}
		$s .= "<td align='center'><a target='_blank' href='$page'><img border=0 src='" . $v['url_sq'] . "' /></a><br/>" . $v['title'] . "</td>" ;
		$cnt++ ;
	} else {
		$total++ ;
		$s = "<tr>" ;
		$s .= "<td><a target='_blank' href='$page'><img border=0 src='" . $v['url_sq'] . "' /></a></td>" ;
		$s .= "<td>" . $v['title'] . "</td><td" ;
		if ( isset ( $pics_in_groups[$pid] ) ) {
			$s .= " bgcolor='#DDDDDD'>In " . count ( $pics_in_groups[$pid] ) . " groups" ;
		} else {
			$s .= "><i>Not in any group!</i>" ;
		}
		$s .= "</td></tr>" ;
	}
	print $s ;
	myflush();
}
print "</table>DONE! ($total pictures)" ;

/*print "<pre>" ;
print_r ( $files ) ;
print "</pre>" ;*/

print "</body></html>" ;

?>
