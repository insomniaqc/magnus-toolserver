<?PHP

include_once ( 'common.php' ) ;

function parse_wikisource_bible ( $bible , $from , $to , $bookname ) {
	$title = "Bible ($bible)/$bookname" ;
	$title = str_replace ( ' ' , '_' , $title ) ;
	$url = "http://en.wikisource.org/w/index.php?title=$title&action=render" ;
	$html = file_get_contents ( $url ) ;
	if ( $html == '' ) return ;
	
	$f = array_pop ( explode ( ':' , $from ) ) ;
	$a = explode ( ' id="'.$from.'"' , $html ) ;
	if ( count ( $a ) != 2 ) return ;
	
	$a = array_pop ( $a ) ;
	$a = array_pop ( explode ( '</span>' , $a , 2 ) ) ;
	
	if ( $from == $to ) {
		$b = array_shift ( explode ( '<' , $a , 2 ) ) ;
	} else {
		$s = ' id="'.$to.'"' ;
		$b = explode ( $s , $a , 2 ) ;
		$p1 = array_shift ( $b ) ;
		$p2 = array_pop ( $b ) ;
		$p2 = explode ( '</span>' , $p2 , 2 ) ;
		$p3 = array_pop ( $p2 ) ;
		$p2 = array_shift ( $p2 ) ;
		$p3 = array_shift ( explode ( '<' , $p3 , 2 ) ) ;
		$b = $p1 . $s . $p2 . '</span>' . $p3 ;
	}
	
	$b = str_replace ( '<p>' , '' , $b ) ;
	$b = str_replace ( '</p>' , '' , $b ) ;
	$b = str_replace ( '<span' , '<br/><span' , $b ) ;
	$b = '<span style="color: rgb(0, 0, 255);"><sup>'.$f.'</sup></span>' . $b ;
	
	$link = "<a href='http://en.wikisource.org/wiki/$title#$from'>$bible</a>" ;
	
	print "<tr><th>$link</th><td>$b</td></tr>" ;
	myflush() ;
}

function parse_greek_bible ( $from , $to , $bookname ) {
	$title = str_replace ( ' ' , '_' , $bookname ) ;
	$url = "http://en.wikisource.org/w/api.php?action=query&prop=langlinks&titles=Bible_%28King_James%29/$title&format=php" ;
	$data = unserialize ( file_get_contents ( $url ) ) ;
	$data = array_shift ( $data['query']['pages'] ) ;
	if ( !isset ( $data ) ) return ;
	
	$data = $data['langlinks'] ;
	if ( !isset ( $data ) ) return ;
	
	$el = '' ;
	foreach ( $data AS $d ) {
		if ( $d['lang'] != 'el' ) continue ;
		$el = $d['*'] ;
		break ;
	}
	if ( $el == '' ) return ;
	
	$uel = urlencode ( str_replace ( ' ' , '_' , $el ) ) ;
	$url = "http://el.wikisource.org/w/index.php?action=render&title=$uel" ;
	$html = file_get_contents ( $url ) ;
	if ( $html == '' ) return ;
	
	$main = array_shift ( explode ( ':' , $from ) ) ;
	$hp = explode ( '</h2>' , $html ) ;
	$section = $hp[$main-1] ;
	$html = $hp[$main] ;
	
	$min1 = array_pop ( explode ( ':' , $from ) ) ;
	$min2 = array_pop ( explode ( ':' , $to ) ) ;
	$out = array () ;
	for ( $a = $min1 ; $a <= $min2 ; $a++ ) {
		$n = array_pop ( explode ( '>' . $a . '</span>' , $html , 2 ) ) ;
		$n = array_shift ( explode ( '<' , $n , 2 ) ) ;
		$out[] = '<span style="color: rgb(0, 0, 255);"><sup>'.$a.'</sup></span>' . $n ;
	}
	$out = implode ( '<br/>' , $out ) ;
	
	$section = array_pop ( explode ( 'name="' , $section ) ) ;
	$section = array_shift ( explode ( '"' , $section ) ) ;
	
	
	$link = "<a href=\"http://el.wikisource.org/wiki/$uel#$section\">Greek original</a>" ;
	
	print "<tr><th>$link</th><td>$out</td></tr>" ;
	myflush() ;
}

function parse_lolcat_bible ( $bible , $from , $to , $bookname ) {
	$main = array_shift ( explode ( ':' , $from ) ) ;
	$title = "$bookname $main" ;
	$title = str_replace ( ' ' , '_' , $title ) ;
	$url = "http://www.lolcatbible.com/index.php?title=$title&action=render" ;
	$html = file_get_contents ( $url ) ;
	if ( $html == '' ) return ;

	$min1 = array_pop ( explode ( ':' , $from ) ) ;
	$min2 = array_pop ( explode ( ':' , $to ) ) ;
	$out = array () ;
	for ( $a = $min1 ; $a <= $min2 ; $a++ ) {
		$n = array_pop ( explode ( 'href="#' . $a . '"' , $html , 2 ) ) ;
		$n = array_pop ( explode ( '>' , $n , 2 ) ) ;
		$n = array_shift ( explode ( '</span>' , $n , 2 ) ) ;
		$out[] = '<span style="color: rgb(0, 0, 255);"><sup>'.$a.'</sup></span>' . $n ;
	}
	$out = implode ( '<br/>' , $out ) ;
	
	$link = "<a href='http://www.lolcatbible.com/index.php?title=$title#$min1'>LOLCAT</a>" ;
	
	print "<tr><th>$link</th><td>$out</td></tr>" ;
	myflush() ;
}


$scriptname = array_pop ( explode ( '/' , $_SERVER["SCRIPT_NAME"] ) ) ;
$bookname = ucfirst ( get_request ( 'bookname' , '' ) ) ;
$booknumber = strtolower ( get_request ( 'booknumber' , '' ) ) ;
$range = strtolower ( get_request ( 'range' , '' ) ) ;
$source = strtolower ( get_request ( 'source' , '' ) ) ;

$lolcat = isset ( $_REQUEST['lolcat'] ) ;
$bibles = array ( 'lolcat' , 'greek' , 'American Standard' , 'Free' , 'King James' , 'Tyndale' , 'World English' , 'Wycliffe' , 'Wikisource' ) ;

print "<html><body>" ;
print '<head><meta http-equiv="Content-Type" content="text/html; charset=utf-8" /></head>' ;
print get_common_header ( $scriptname ) ;
print "<h1>BibleBay</h1>
<form method='get' action='./$scriptname'>
<table border=1>
<tr><th>Book number</th><td><input type='text' name='booknumber' value='$booknumber' /></td></tr>
<tr><th>Book name</th><td><input type='text' name='bookname' value='$bookname' /> (required)</td></tr>
<tr><th>Range</th><td><input type='text' name='range' value='$range' /> (required)</td></tr>
<tr><th>Source</th><td><input type='text' name='source' value='$source' /></td></tr>
<tr><th/><td><input type='submit' name='doit' value='Do it' /></td></tr>
</table>
</form>
" ;

if ( $bookname != '' && $range != '' ) {
	print "<h2>$bookname $range</h2>" ;
	print "<table border=1>" ;
	
	$n = explode ( '-' , $range ) ;
	if ( count ( $n ) == 1 ) $n[] = $range ;
	$from = trim ( $n[0] ) ;
	$to = trim ( $n[1] ) ;
	if ( preg_match ( '/^\d+$/' , $to ) ) $to = array_shift ( explode ( ':' , $from ) ) . ":$to" ;

	
	foreach ( $bibles AS $bible ) {
		if ( $bible == 'lolcat' ) {
			if ( !$lolcat ) continue ;
			parse_lolcat_bible ( $bible , $from , $to , $bookname ) ;
		} else if ( $bible == 'greek' ) {
			parse_greek_bible ( $from , $to , $bookname ) ;
		} else {
			parse_wikisource_bible ( $bible , $from , $to , $bookname ) ;
		}
	}
	print "</table>" ;
	
	$t = $bookname . $from ;
	if ( $from != $to ) {
		$min2 = array_pop ( explode ( ':' , $to ) ) ;
		$t .= "-$min2" ;
	}
	$url = "http://www.ibsstl.org/bible/verse/?q=$t&niv=yes" ;
	print "Try also the <a href='$url'>New International Version</a>." ;
}

print '</body></html>' ;

?>