<?PHP

include_once ( "common.php" ) ;

$language = get_request ( 'language' , 'en' ) ;
$project = get_request ( 'project' , 'wikipedia' ) ;
$user1 = get_request ( 'user1' , '' ) ;
$user2 = get_request ( 'user2' , '' ) ;

function cmp ( $a , $b ) {
   if ($a->editnum == $b->editnum) {
        return 0;
    }
    return ($a->editnum > $b->editnum) ? -1 : 1;
}

print '<html><head><meta http-equiv="Content-Type" content="text/html;charset=UTF-8"></head><body>' ;
print get_common_header ( "familiar.php" , "Familiar" ) ;
print "<h1>Familiar</h1>A rewrite of the tool by cyroxx." ;

print "<form method='get' acrion='./familar.php'><table border='1'>
<tr>
<th>Language</th><td><input type='text' name='language' value='$language' /></td>
<th>Project</th><td><input type='text' name='project' value='$project' /></td>
</tr>
<tr>
<th>User 1</th><td><input type='text' name='user1' value='$user1' /></td>
<th>User 2</th><td><input type='text' name='user2' value='$user2' /></td>
</tr>
<tr><td colspan='4'><input type='submit' name='doit' value='Do it' /></td></tr>
</table></form>" ;

if ( $user1 != '' && $user2 != '' && isset ( $_REQUEST['doit'] ) ) {
	$uid1 = db_get_user_id ( $language , $user1 , $project ) ;
	$uid2 = db_get_user_id ( $language , $user2 , $project ) ;
	
	$mysql_con = db_get_con_new ( $language , $project ) ;
	$db = get_db_name ( $language , $project ) ;
	$sql = "select count(*) AS editnum,rev_page,count(distinct rev_user) as usernum,page_namespace,page_title from page,revision where rev_page=page_id and rev_user in ( $uid1,$uid2 ) group by rev_page having usernum=2" ;
	$res = my_mysql_db_query ( $db , $sql , $mysql_con ) ;
	$common_edits = array () ;
	while ( $o = mysql_fetch_object ( $res ) ) {
		$common_edits[] = $o ;
	}
	usort ( $common_edits , "cmp" ) ;
	
	$nsdata = unserialize ( file_get_contents ( "http://$language.$project.org/w/api.php?action=query&meta=siteinfo&siprop=namespaces&format=php" ) ) ;
	$nsdata = (array) $nsdata['query']['namespaces'] ;
	
	print "<br/><h2>" . count ( $common_edits ) . " pages edited by both $user1 and $user2</h2>" ;
	print "<table border='1'>" ;
	print "<tr><th>Namespace</th><th>Title</th><th>Cumulative edits by both users</th></tr>" ;
	foreach ( $common_edits AS $k => $c ) {
		$nsname = $nsdata[$c->page_namespace]["*"] ;
		$url = "http://$language.$project.org/wiki/" ;
		if ( $nsname != '' ) $url .= "$nsname:" ;
		$url .= $c->page_title ;
		$ns = str_replace ( '_' , ' ' , $c->page_title ) ;
		$s = '' ;
		if ( $c->page_namespace % 2 == 1 ) $s .= ';background-color:#DDDDDD' ;
		if ( $c->page_namespace == 0 ) $s .= ';background-color:#BBFFBB' ;
		if ( $s != '' ) $s = " style='$s'" ;
		print "<tr>" ;
		print "<td$s>$nsname</td>" ;
		print "<td$s><a target='_blank' href='$url'>$ns</a></td>" ;
		print "<td$s>" . $c->editnum . "</td>" ;
		print "</tr>" ;
	}
	print "</table>" ;
}

print "</body></html>" ;

?>