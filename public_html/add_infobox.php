<?php

$hide_header = false ;
$hide_doctype = false ;
include_once ( 'queryclass.php' ) ;
require_once("xml2json.php");

$language = fix_language_code ( get_request ( 'language' , 'en' ) , 'en' ) ;
$project = check_project_name ( get_request ( 'project' , 'wikipedia' ) ) ;
$project2 = $project ;
if ( $language == 'commons' ) $project2 = 'wikimedia' ;
$page = get_request ( 'page' ,  '' ) ;
$infobox = get_request ( 'infobox' ,  '' ) ;
$page = str_replace ( '_' , ' ' , $page ) ;

$doit = isset ( $_REQUEST['doit'] ) ;

//$wq = new WikiQuery ( $language , $project ) ;

function look_through_json ( $json , $type = '' ) {
	if ( !is_object ( $json ) ) return ;
	
	if ( $type == 'template' and $json->target == 'Persondata' ) {
		print "$type : $k\n" ;
		return ;
	}
	
	foreach ( $json AS $k => $v ) {
		if ( is_array ( $v ) ) {
			foreach ( $v AS $v2 ) {
				look_through_json ( $v2 , $k ) ;
			}
		} else {
			look_through_json ( $v , $k ) ;
		}
	}
}

function add_infobox_scientist () {
	global $page ;
	$url = "http://toolserver.org/~magnus/wiki2xml/w2x.php?doit=1&whatsthis=articlelist&site=en.wikipedia.org/w&output_format=xml&text=" . urlencode ( $page ) . "&use_templates=none" ;
	$xmlStringContents = file_get_contents ( $url ) ;
	$jsonContents = xml2json::transformXmlStringToJson($xmlStringContents);
	$json = json_decode ( $jsonContents ) ;

	header('Content-type: text/plain; charset=utf-8');
	
	look_through_json ( $json->articles->article ) ;
	
	print_r ( $json ) ;
//	print $jsonContents ;
//	print json_encode ( $json , JSON_PRETTY_PRINT | JSON_FORCE_OBJECT ) ;
}

// Output

if ( $doit ) {
	if ( $infobox == 'scientist' ) add_infobox_scientist () ;
} else {
	header('Content-type: text/html; charset=utf-8');
	print '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">' . "\n\n" ;
	print "<html><head></head>" ;
	print "<body>" ;
	print "<form method='get' action='http://toolserver.org/~magnus/add_infobox.php'>
<table border=1>
<tr><th>Language</th><td><input type='text' name='language' value='$language' /></td></tr>
<tr><th>Project</th><td><input type='text' name='project' value='$project' /></td></tr>
<tr><th>Page</th><td><input type='text' name='page' value='$page' /></td></tr>
<tr><th>Mode</th><td><select name='infobox'><option value='scientist'>Scientist</option></select></td></tr>
<tr><td /><td><input type='submit' name='doit' value='Do it' /></td></tr>
</table>
</form>" ;
print "</body></html>" ;
}




?>
