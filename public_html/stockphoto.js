/*
* StockPhoto - shows download and attribution buttons
* Original code 2010-09/10 by [[User:Magnus Manske]]
*/
if (typeof StockPhoto == 'undefined' && wgNamespaceNumber == 6 && wgAction == 'view' && getParamValue('diff') === null/* && getParamValue('stockphoto_off') === null*/ && typeof $j == 'function' ) {
	// jQuery UI is not loaded on all pages:
	if (jQuery.ui == undefined){
		importStylesheetURI( wgServer + '/w/extensions/UsabilityInitiative/css/vector/jquery-ui-1.7.2.css' );
		importScriptURI( wgServer + '/w/extensions/UsabilityInitiative/js/js2stopgap/jui.combined.min.js');
	}

	var StockPhoto = {
	
		// Config
		show_social_bookmarks : true ,
		small_horizontal_layout : true ,

		// Misc
		ui_icon_download : 'http://upload.wikimedia.org/wikipedia/commons/thumb/9/92/Gnome-document-save.svg/50px-Gnome-document-save.svg.png' ,
		ui_icon_web : 'http://upload.wikimedia.org/wikipedia/commons/thumb/c/c0/Gnome-emblem-web.svg/50px-Gnome-emblem-web.svg.png' ,
		ui_icon_wiki : 'http://upload.wikimedia.org/wikipedia/commons/thumb/c/c0/Wiki_puzzle.svg/50px-Wiki_puzzle.svg.png' ,
		ui_icon_email : 'http://upload.wikimedia.org/wikipedia/commons/thumb/e/ee/Gnome-mail-send.svg/50px-Gnome-mail-send.svg.png' ,
		ui_icon_help : 'http://upload.wikimedia.org/wikipedia/commons/thumb/e/eb/Information_icon_with_gradient_background.svg/50px-Information_icon_with_gradient_background.svg.png' ,
		information_template_hints : [ 'fileinfotpl_desc' , 'fileinfotpl_src' ] ,
		icons_only : [ "midi" , "ogg" , "flac" ] ,
		horizontal_ui : [ "midi" , "ogg" , "flac" , 'pdf' , 'djvu' ] ,
		license_patterns : [ "^CC-" , "^GFDL" , "^GPL$" , "^LGPL$" , "^PD[- ]" , "^Public domain" , "^Artistic-2$" , "^BSD images$" , "^OS OpenData$" , "^FAL$" , "^Mozilla Public License$" ] ,
		stockphoto_css_list : 'border-bottom:2px solid #DDDDDD;margin-bottom:5px' ,
		ogg_icon : 'http://bits.wikimedia.org/skins-1.5/common/images/icons/fileicon-ogg.png' ,
		stockphoto_code1 : undefined ,
		stockphoto_author : undefined ,
		stockphoto_license : undefined ,
		nicetitle : undefined ,
		file_icon : undefined ,
		file_url : undefined ,
		backlink_url : undefined ,
		attribution : '' ,
		fade_target : '' ,
		allows_gfdl : false ,
		sbm_counter : 1 ,

		init : function () {
			if ( typeof stockphoto_prevent != "undefined" ) return ;
			var has_information = false ;
			$j.each ( StockPhoto.information_template_hints , function ( k , v ) {
				if ( $j('#'+v).length !== 0 ) has_information = true ;
			} ) ;
			if ( !has_information ) return ; // No {{Information}}

			$j('#stockphoto_base').remove() ;
			var img_width = $j('#file img').width() ;
			var img_height = $j('#file img').height() ;
			var xoff = img_width + 40 ;
			var yoff = $j('#file').position().top + 5 ;

			var horizontal = StockPhoto.small_horizontal_layout ;
			$j.each ( StockPhoto.horizontal_ui , function ( k , v ) {
				v = new RegExp('\.'+v+'$','i');
				if ( wgTitle.match ( v ) ) horizontal = true ;
			} ) ;
			if ( !horizontal && img_height < 230 ) horizontal = true ;
			if ( $j(window).width() < 1030 ) horizontal = true ;

			StockPhoto.share_this ( -1 ) ; // Initialize values
			var html = "<div id='stockphoto_base' style='z-index:99;position:absolute;left:"+xoff+"px;top:" + yoff + "px'>" ;
			if ( horizontal ) html = "<div id='stockphoto_base' style=''>" ;
			html += StockPhoto.add_button_row ( StockPhoto.ui_icon_download , "call_download" , StockPhoto.i18n.download , StockPhoto.i18n.all_sizes , horizontal ) ;
			html += StockPhoto.add_button_row ( StockPhoto.ui_icon_web , "call_web" , StockPhoto.i18n.use_this_file_web_short , StockPhoto.i18n.on_a_website , horizontal ) ;
			html += StockPhoto.add_button_row ( StockPhoto.ui_icon_wiki , "call_wiki" , StockPhoto.i18n.use_this_file_wiki_short , StockPhoto.i18n.on_a_wiki , horizontal ) ;
			html += StockPhoto.add_button_row ( StockPhoto.ui_icon_email , "send_email" , StockPhoto.i18n.email_link_short , StockPhoto.i18n.to_this_file , horizontal ) ;
			html += StockPhoto.add_button_row ( StockPhoto.ui_icon_help , "call_help" , StockPhoto.i18n.information , StockPhoto.i18n.about_reusing , horizontal ) ;
			if ( StockPhoto.show_social_bookmarks ) html += StockPhoto.add_social_icons ( horizontal ) ;
			html += "</div>" ;
			if ( StockPhoto.small_horizontal_layout && $j("#file").length !== 0 ) $j("#file").append ( html ) ;
			else $j("#filetoc").after ( html ) ;
			$j("#stockphoto_base img").fadeTo ( 0 , 0.5 ) ;
		} ,
		
		add_social_icons : function ( horizontal ) {
			var elm = "div" ;
			if ( $j.browser.msie && horizontal ) elm = "span" ;
			var nl = encodeURIComponent("\n") ;
			var bl = encodeURIComponent(StockPhoto.backlink_url) ;
			var fl = encodeURIComponent(StockPhoto.file_url) ;
			var ti = encodeURIComponent(StockPhoto.nicetitle + " - " + StockPhoto.stockphoto_code1+" "+StockPhoto.i18n.from_wikimedia_commons) ;
			var look = encodeURIComponent(StockPhoto.i18n.look_what_i_found)+bl+encodeURIComponent(" / ")+ti ;
			var ret = "<"+elm+" id='stockphoto_social_bookmarks' style='text-align:center;width:160px;margin-botton:10px'" ;
			if ( horizontal ) ret = "<"+elm+" id='stockphoto_social_bookmarks' style='display:inline-block;text-align:center;margin-botton:10px'" ;
//			ret += " onmouseover=\"StockPhoto.fade('stockphoto_social_bookmarks',1)\"" ;
//			ret += " onmouseout=\"StockPhoto.fade('stockphoto_social_bookmarks',0)\"" ;
			ret += ">" ;
			ret += StockPhoto.add_social_icon ( StockPhoto.i18n.share_on_facebook , "http://upload.wikimedia.org/wikinews/en/5/55/Facebook.png" , "http://www.facebook.com/sharer.php?u="+bl+"&q="+ti ) ;
			ret += StockPhoto.add_social_icon ( StockPhoto.i18n.share_on_digg , "http://upload.wikimedia.org/wikinews/en/9/95/Digg-icon.png" , "http://digg.com/submit?url="+bl+"&title="+ti ) ;
			ret += StockPhoto.add_social_icon ( StockPhoto.i18n.share_on_delicious , "http://upload.wikimedia.org/wikipedia/commons/thumb/1/11/Delicious.svg/16px-Delicious.svg.png" , "http://delicious.com/post?url="+bl+"&title="+ti ) ;
			ret += StockPhoto.add_social_icon ( StockPhoto.i18n.share_on_reddit , "http://upload.wikimedia.org/wikinews/en/1/10/Reddit.png" , "http://reddit.com/submit?url="+bl+"&title="+ti ) ;
			ret += StockPhoto.add_social_icon ( StockPhoto.i18n.share_on_stumbleupon , "http://upload.wikimedia.org/wikipedia/commons/thumb/4/41/Stumbleupon.svg/16px-Stumbleupon.svg.png" , "http://stumbleupon.com/submit?url="+bl+"&title="+ti ) ;
			if ( !StockPhoto.small_horizontal_layout ) ret += "<br/>" ;
			ret += StockPhoto.add_social_icon ( StockPhoto.i18n.share_on_yahoo_buzz , "http://upload.wikimedia.org/wikinews/en/d/de/Yahoo_buzz.png" , "http://buzz.yahoo.com/buzz?targetUrl="+bl+"&headline="+ti ) ;
			ret += StockPhoto.add_social_icon ( StockPhoto.i18n.share_on_identi_ca , "http://upload.wikimedia.org/wikipedia/commons/thumb/f/f1/Identica_Icon.png/16px-Identica_Icon.png" , "http://identi.ca/index.php?action=newnotice&status_textarea="+look ) ;
			ret += StockPhoto.add_social_icon ( StockPhoto.i18n.share_on_google_buzz , "http://upload.wikimedia.org/wikinews/en/4/4e/Buzz_icon.png" , "http://www.google.com/buzz/post?imageurl="+fl+"&message="+bl+nl+ti ) ;
			ret += StockPhoto.add_social_icon ( StockPhoto.i18n.share_on_twitter , "http://upload.wikimedia.org/wikinews/en/f/f7/Twitter.png" , "http://twitter.com/share?url=" + bl + "&text=" + encodeURIComponent(StockPhoto.i18n.look_what_i_found) + ti + "&related=WM_POTD" ) ;
			ret += StockPhoto.add_social_icon ( StockPhoto.i18n.share_on_newstrust , "http://upload.wikimedia.org/wikinews/en/0/00/Newstrust.gif" , "http://newstrust.net/submit?story%5Burl%5D="+bl+"&story%5Bauthorships_attributes%5D%5B%5D%5Bname%5D=Wikimedia Commons&story%5Breferred_by%5D=commons.wikimedia.org&popup=true" ) ;
			ret += "</"+elm+">" ;
			return ret ;
		} ,
		
		add_social_icon : function ( title , image_url , target_url ) {
			var sbmid = "stockphoto_social_bookmarks_" + StockPhoto.sbm_counter ;
			StockPhoto.sbm_counter++ ;
			var ret = "<span style='padding-right:2px' id='" + sbmid + "'" ;
			ret += " onmouseover=\"StockPhoto.fade('"+sbmid+"',1)\"" ;
			ret += " onmouseout=\"StockPhoto.fade('"+sbmid+"',0)\"" ;
			ret += ">" ;
			ret += "<a href=\"" + target_url + "\" target='_blank' ><img title=\"" + title + "\" border='0' src=\"" + image_url + "\" /></a>" ;
			ret += "</span>" ;
			return ret ;
		} ,

		add_button_row : function ( icon_url , fkt , txt , html , horizontal ) {
			if ( StockPhoto.small_horizontal_layout ) {
				icon_url = icon_url.replace ( /\/50px-/ , '/20px-' ) ;
			}
			var imgid = "stockphoto_icon_" + fkt ;
			var triggers = "onmouseover=\"StockPhoto.fade('" + imgid + "',1)\"" ;
			triggers += " onmouseout=\"StockPhoto.fade('" + imgid + "',0)\"" ;
			var a = "<a href='#' title=\"" + html + "\" onclick='StockPhoto." + fkt + "();return false;'>" ;
			var boxw = StockPhoto.small_horizontal_layout ? '' : 'width:160px;height:55px;' ;
			var iboxw = StockPhoto.small_horizontal_layout ? '' : 'width:60px;' ;
			var elm_outer = "div" ;
			var elm_inner = "div" ;
			if ( $j.browser.msie ) {
				elm_inner = "span" ;
				if ( horizontal ) elm_outer = "span" ;
			}
			var ret = "<"+elm_outer+" id='stockphoto_" + fkt + "' style='" + boxw + "margin-botton:10px' " + triggers + ">" ; // Vertical
			if ( horizontal ) ret = "<"+elm_inner+" style='display:inline-block;" + boxw + "margin-right:10px' " + triggers + ">" ;
			ret += "<"+elm_inner+" style='" + iboxw + "display:inline-block'>" + a + "<img id='"+imgid+"' border=0 src=\"" + icon_url + "\"></a></"+elm_inner+">" ;
			ret += "<"+elm_inner+" style='vertical-align:middle;text-align:center;display:inline-block'>" + a ;
			if ( StockPhoto.small_horizontal_layout ) ret += txt + "</a>" ;
			else ret += "<b>" + txt + "</b></a><br/>" + html ;
			ret += "</"+elm_inner+"></"+elm_outer+">" ;
			return ret ;
		} ,

		stockphoto_get_thumbnail_url : function ( width ) {
			if ( StockPhoto.isset ( StockPhoto.file_icon ) ) return StockPhoto.file_icon ;
			var thumb_url ;
			var alt_title = wgCanonicalNamespace + ":" + wgTitle ;
			$j('#file img').each ( function () {
				if ( $j(this).attr('alt') != alt_title ) return ;
				thumb_url = $j(this).attr('src').split('/') ;
			} ) ;
			
			var last = thumb_url.pop().replace(/^\d+px-/,width+'px-') ;
			thumb_url.push ( last ) ;
			thumb_url = thumb_url.join ( '/' ) ;
			return thumb_url ;
		} ,

		make_html_textarea : function () {
			var width = $j('#stockphoto_html_select').val() ;
			var type = $j("input[name='stockphoto_code_type']:checked").val() ;

			var thumb_url = StockPhoto.stockphoto_get_thumbnail_url ( width ) ;

			var t ;
			if ( type == "html" ) t = "<a title=\""+StockPhoto.escapeAttribute(StockPhoto.stockphoto_code1)+" "+StockPhoto.i18n.from_wikimedia_commons+"\" href=\"" + StockPhoto.backlink_url + "\"><img width='"+width+"px' alt=\""+StockPhoto.escapeAttribute(StockPhoto.nicetitle)+"\" src=\""+thumb_url+"\" border='0' /></a>" ;
			else if ( type == "bbcode" ) t = "[url="+StockPhoto.backlink_url+"][img]"+thumb_url+"[/img][/url]\n[url="+StockPhoto.backlink_url+"]"+StockPhoto.nicetitle+"[/url]"+StockPhoto.stockphoto_license+", "+StockPhoto.i18n.by+" "+StockPhoto.stockphoto_author+", "+StockPhoto.i18n.from_wikimedia_commons ;
			$j('#stockphoto_html').text ( t ) ;
		} ,

		get_author_attribution : function ( use_html ) {
			var author = StockPhoto.trim ( $j("#fileinfotpl_aut + td").text() ) ;
			var source = StockPhoto.trim ( $j("#fileinfotpl_src + td").text() ) ;
			if ( author.match ( /This file is lacking author information/ ) ) author = '' ; // Remove boiler template; not elegant, but...
			if ( author.match ( /^[Uu]nknown$/ ) ) author = '' ;
			author = author.replace ( /\s*\(talk\)$/i , "" ) ;
			author = author.replace ( /\s*Original uploader was\s*/g , "" ) ;
			if ( source.match ( /This file is lacking source information/ ) ) source = '' ; // Remove boiler template; not elegant, but...
			if ( author !== '' && source.match(/^\s*own work\s*$/i) ) source = '' ; // Remove "own work" notice
			if ( author !== '' && source.length > 50 ) source = '' ; // Remove long source info
			if ( author.substr ( 0 , 3 ) == "[▼]" ) {
				author = author.substr ( 3 ) ;
				author = StockPhoto.trim ( author.split("Description").shift() ) ;
			}

			StockPhoto.attribution = '' ;
			if ( author !== '' ) StockPhoto.attribution = author ;
			if ( source != '' ) {
				if ( StockPhoto.attribution != '' ) StockPhoto.attribution += " (" + source + ")" ;
				else StockPhoto.attribution = source ;
			}
			StockPhoto.stockphoto_author = StockPhoto.attribution ;
			if ( author !== '' ) StockPhoto.attribution = StockPhoto.i18n.by_u + " " + StockPhoto.attribution ;
			else StockPhoto.attribution = StockPhoto.i18n.see_page_for_author ;
			
			if ( $j("#fileinfotpl_credit + td").length > 0 ) {
				if ( use_html ) StockPhoto.attribution = $j("#fileinfotpl_credit + td").html() ;
				else StockPhoto.attribution = $j("#fileinfotpl_credit + td").text() ;
			}

		} ,

		get_license : function ( generate_html ) {
			var licenses = new Array () ;
			$j("#mw-hidden-catlinks a").each ( function (k1,v1) {
				var cn = $j(v1).text() ;
				var is_license = false ;
				$j.each(StockPhoto.license_patterns,function(i,v){
					v = new RegExp(v,'i');
					if ( !cn.match ( v ) ) return ;
					is_license = true ;
				});
				if ( !is_license ) return ;
				if ( cn.match ( /^public domain/i ) ) cn = "public domain" ;
				if ( cn.match ( /^PD[ -]/i ) ) cn = "public domain" ;
				cn = cn.replace ( /-migrated$/i , '' ) ;
				cn = cn.replace ( /-self$/i , '' ) ;
				if ( $j.inArray ( cn , licenses ) == -1 ) licenses.push ( cn ) ;
			} ) ;

			StockPhoto.stockphoto_license = "["+StockPhoto.i18n.see_page_for_license+"]" ;
			if ( licenses.length > 0 ) {
				licenses = licenses.sort() ;

				$j.each ( licenses , function ( k , v ) {
					if ( v.match ( /^GFDL/i ) ) StockPhoto.allows_gfdl = true ;
					if ( generate_html ) {
						if ( v.match ( /^GFDL/i ) ) {
							licenses[k] = "<a href=\"http://en.wikipedia.org/wiki/GNU_Free_Documentation_License\">"+v+"</a>" ;
						} else if ( v.match ( /^CC-/i ) ) {
							var v2 = v.replace ( /-[-0123456789.]+$/ , '' ) . replace ( /^CC-/ , '' ) . toLowerCase () ;
							licenses[k] = "<a href=\"http://creativecommons.org/licenses/"+v2+"/3.0/\">"+v+"</a>" ; // Can't find a way to link to generically link to a license, so 3.0 should be valid anyway...
						}
					}
				} ) ;

				if ( licenses.length > 1 ) {
					var l2 = licenses.pop () ;
					var l1 = licenses.pop () ;
					licenses.push ( l1 + " or " + l2 ) ;
				}
				
				StockPhoto.stockphoto_license = " [" + licenses.join(', ') + "]" ;
			}
		} ,

		get_attribution_text : function () {
			var s = StockPhoto.stockphoto_code1 ;
			if ( $j("#stockphoto_attribution_html:checked").length > 0 ) {
				var old = StockPhoto.stockphoto_license ;
				StockPhoto.get_license ( true ) ;
				s = StockPhoto.stockphoto_license + "," ;
				StockPhoto.stockphoto_license = old ;
				
				old = StockPhoto.attribution ;
				StockPhoto.get_author_attribution ( true ) 
				if ( $j("#fileinfotpl_credit + td").length > 0 ) s = StockPhoto.attribution ;
				else s = StockPhoto.attribution + s ;
				StockPhoto.attribution = old ;
				
				s += " <a href=\"" + StockPhoto.escapeAttribute(StockPhoto.backlink_url) + "\">"+StockPhoto.i18n.from_wikimedia_commons+"</a>" ;
			} else {
				s += " " + StockPhoto.i18n.from_wikimedia_commons ;
			}
			return s ;
		} ,
		
		refresh_attribution : function () {
			$j("#stockphoto_attribution").val ( StockPhoto.get_attribution_text() ) ;
		} ,
		
		share_this : function ( ui_mode ) {
			StockPhoto.get_author_attribution ( false ) ;

			StockPhoto.file_url = $j("#file > a").attr("href") ;
			if ( !StockPhoto.file_url ) StockPhoto.file_url = $j("#file > div > div > a").attr("href") ;

			StockPhoto.nicetitle = wgTitle.split('.') ;
			StockPhoto.nicetitle.pop() ;
			StockPhoto.nicetitle = StockPhoto.nicetitle.join('.') ;

			$j.each ( StockPhoto.icons_only , function ( i , v ) {
				var re = new RegExp('\.'+v+'$','i');
				if ( !wgPageName.match ( re ) ) return ;
				StockPhoto.file_icon = StockPhoto.ogg_icon ;
			} ) ;

			StockPhoto.backlink_url = "http://commons.wikimedia.org/wiki/" + wgPageName ;

			StockPhoto.get_license ( false ) ;
			StockPhoto.stockphoto_code1 = StockPhoto.attribution + StockPhoto.stockphoto_license + "," ;

			var widths = [ 75 , 100 , 120 , 240 , 500 , 640 , 800 , 1024 ] ;

			if ( ui_mode == -1 ) return ;

			var html = "" ;
			html += "<div style='"+StockPhoto.stockphoto_css_list+"'><b>"+StockPhoto.i18n.page_url+":</b><br/><input type='text' size='100' value=\"" + StockPhoto.escapeAttribute(StockPhoto.backlink_url) + "\" /></div>" ;
			html += "<div style='"+StockPhoto.stockphoto_css_list+"'><b>"+StockPhoto.i18n.file_url+":</b><br/><input type='text' size='100' value=\"" + StockPhoto.escapeAttribute(StockPhoto.file_url) + "\" /></div>" ;
			html += "<div style='"+StockPhoto.stockphoto_css_list+"'><b>"+StockPhoto.i18n.attribution+":</b><br/><input type='text' size='100' id='stockphoto_attribution' value=\"" + StockPhoto.escapeAttribute(StockPhoto.get_attribution_text()) + "\" />" ;
			html += " <input id='stockphoto_attribution_html' onclick='StockPhoto.refresh_attribution()' type='checkbox' /><label for='stockphoto_attribution_html'>"+StockPhoto.i18n.html+"</label>" ;
			if ( StockPhoto.allows_gfdl ) html += "<br/><i>"+StockPhoto.i18n.gfdl_warning+"</i>" ;
			html += "</div>" ;

			if ( ui_mode == 2 ) {
				html += "<div style='"+StockPhoto.stockphoto_css_list+"'><div style='float:right'>" ;
				html += "<input type='radio' name='stockphoto_code_type' value='html' id='stockphoto_code_type_html' onchange='StockPhoto.make_html_textarea();return false' checked /><label for='stockphoto_code_type_html'>"+StockPhoto.i18n.html+"</label> " ;
				html += "<input type='radio' name='stockphoto_code_type' value='bbcode' id='stockphoto_code_type_bbcode' onchange='StockPhoto.make_html_textarea();return false' /><label for='stockphoto_code_type_bbcode'>"+StockPhoto.i18n.bbcode+"</label> " ;

				html += "<select id='stockphoto_html_select' onchange='StockPhoto.make_html_textarea();return false'>" ;
				var best_fit = 75 ;
				if ( StockPhoto.isset ( StockPhoto.file_icon ) ) {
					best_fit = 120 ;
					html += "<option value='120'>120" + StockPhoto.i18n.px_wide_icon + "</option>" ;
				} else {
					$j.each ( widths , function ( i , v ) {
						if ( v <= $j('#file img').width() ) best_fit = v ;
						html += "<option value='"+v+"'>"+v+StockPhoto.i18n.px_wide+"</option>" ;
					} ) ;
				}
				html += "</select></div>" ;
				html += "<b>"+StockPhoto.i18n.html+"/"+StockPhoto.i18n.bbcode+":</b><textarea id='stockphoto_html' cols='80' rows='5' style='font-size:8pt'>" ;
				html += "</textarea></div>" ;
			}

			if ( ui_mode == 3 ) {
				html += "<div style='"+StockPhoto.stockphoto_css_list+"'><b>"+StockPhoto.i18n.wikipedia_instant_commons+"<a target='_blank' href='http://www.mediawiki.org/wiki/InstantCommons'><sup>?</sup></a>:</b><table border='0'>" ;
				html += "<tr><td>Thumbnail</td><td><input size='70' type='text' value=\""+StockPhoto.escapeAttribute("[[File:"+wgTitle+"|thumb|"+StockPhoto.nicetitle+"]]")+"\" /></td></tr>" ;
				html += "<tr><td>Image</td><td><input size='70' type='text' value=\""+StockPhoto.escapeAttribute("[[File:"+wgTitle+"|"+StockPhoto.nicetitle+"]]")+"\" /></td></tr>" ;
				html += "</table></div>" ;
			}

			if ( ui_mode == 1 ) {
				if ( StockPhoto.isset ( StockPhoto.file_url ) ) {
					html += "<div><b>"+StockPhoto.i18n.download_image_file+":</b><br>" ;
					var dl_links = new Array () ;
					$j.each ( widths , function ( i , v ) {
						if ( StockPhoto.isset ( StockPhoto.file_icon ) ) return ;
						dl_links.push ( "<a href=\"" + StockPhoto.stockphoto_get_thumbnail_url ( v ) + "\">" + v + "px</a>" ) ;
					} ) ;
					if ( StockPhoto.file_url ) dl_links.push ( "<a href=\"" + StockPhoto.file_url + "\">"+StockPhoto.i18n.full_resolution+"</a>" ) ;
					if ( dl_links.length > 0 ) html += dl_links.join ( " | " ) ;
					else html += "<i>"+StockPhoto.i18n.not_available+"</i>" ;
					html += "</div>" ;
				}
			}

			$j('#stockphoto_base').append ( "<div style='display:none' id='stockphoto_dialog'><div style='font-size:10pt'>" + html + "</div></div>" ) ;
			$j('#stockphoto_html_select').val(best_fit) ;
			StockPhoto.make_html_textarea() ;
			var dtitle = StockPhoto.i18n.share_this_file ;
			if ( ui_mode == 1 ) dtitle = StockPhoto.i18n.download_this_file ;
			if ( ui_mode == 2 ) dtitle = StockPhoto.i18n.use_this_file_web ;
			if ( ui_mode == 3 ) dtitle = StockPhoto.i18n.use_this_file_wiki ;
			$j("#stockphoto_dialog").dialog( { modal : true , width : "auto" , height : "auto" , title : dtitle , close: function(ev, ui) { $j(this).remove(); } } ) ;
			$j("#stockphoto_dialog a").css ( { color : 'blue' } ) ;
		} ,

		call_download : function () {
			StockPhoto.share_this ( 1 ) ;
		} ,

		call_web : function () {
			StockPhoto.share_this ( 2 ) ;
		} ,

		call_wiki : function () {
			StockPhoto.share_this ( 3 ) ;
		} ,

		call_help : function () {
			window.location.href = StockPhoto.i18n.reusing_content_url ;
		} ,
		
		send_email : function () {
			var url = "mailto:?subject="+encodeURIComponent(StockPhoto.nicetitle)+"&body="+encodeURIComponent(StockPhoto.backlink_url+"\n\n"+StockPhoto.stockphoto_code1+" "+StockPhoto.i18n.from_wikimedia_commons) ;
			window.location.href = url ;
		} ,

		escapeAttribute : function (s) {
			if ( typeof s  == "undefined" ) return "" ;
			return s.replace(/\n/g, ' ').replace(/\r/g, ' ').replace(/"/g, '&quot;');
		} ,

		trim : function ( s ) {
			if ( typeof s  == "undefined" ) return "" ;
			return s.replace(/^\s+/, '').replace(/\s+$/, '');
		} ,

		isset : function ( v ) {
			return ( typeof ( v ) != 'undefined' ) ;
		} ,

		fade : function ( id , mode ) {
			var key = id + "_" + mode ;
			if ( key == StockPhoto.fade_target ) return ;
			StockPhoto.fade_target = key ;
			if ( mode == 1 ) {
				$j("#"+id).fadeTo ( 0 , 1 ) ;
				$j("#"+id+" img").fadeTo ( 0 , 1 ) ;
			} else {
				$j("#"+id).fadeTo ( 0 , 0.5 ) ;
				$j("#"+id+" img").fadeTo ( 0 , 0.5 ) ;
			}
		} ,
		
		i18n : {
			download : 'Download' ,
			download_this_file : "Download this file" ,
			use_this_file_web : "Use this file on the web" ,
			use_this_file_web_short : "Use this file" ,
			use_this_file_wiki : "Use this file on a wiki" ,
			use_this_file_wiki_short : "Use this file" ,
			email_link_short : "Email a link" ,
			information : "Information" ,
			all_sizes : "all sizes" ,
			on_a_website : "on a website" ,
			on_a_wiki : "on a wiki" ,
			to_this_file : "to this file" ,
			about_reusing : "about reusing" ,
			look_what_i_found : "Look what I found on Wikimedia Commons : " ,
			from_wikimedia_commons : "from Wikimedia Commons" ,
			by : "by" ,
			by_u : "By" ,
			see_page_for_author : "See page for author" ,
			see_page_for_license : "see page for license" ,
			page_url : "Page URL" ,
			file_url : "File URL" ,
			attribution : "Attribution" ,
			gfdl_warning : "Using this file might require attaching a full copy of the <a href='http://en.wikipedia.org/wiki/GNU_Free_Documentation_License'>GFDL</a>" ,
			download_image_file : "Download image file" ,
			full_resolution : "Full resolution" ,
			not_available : "not available" ,
			share_this_file : "Share this file" ,
			html : "HTML" ,
			bbcode : "BBCode" ,
			px_wide_icon : "px wide (icon)" ,
			px_wide : "px wide" ,
			wikipedia_instant_commons : "Wikimedia/InstantCommons" ,
			share_on_facebook : "Bookmark with Facebook" ,
			share_on_digg : "Share on Digg.com" ,
			share_on_delicious : "Share on delicious" ,
			share_on_reddit : "Share on reddit.com" ,
			share_on_stumbleupon : "Share on stumbleupon.com" ,
			share_on_yahoo_buzz : "Share on Yahoo! Buzz" ,
			share_on_identi_ca : "Share on identi.ca" ,
			share_on_google_buzz : "Share on Google Buzz" ,
			share_on_twitter : "Share on twitter.com" ,
			share_on_newstrust : "Review on NewsTrust" ,
			reusing_content_url : "http://commons.wikimedia.org/wiki/Commons:Reusing_content_outside_Wikimedia"
		}

	}

	if (wgUserLanguage != 'en') importScript( 'MediaWiki:Stockphoto.js/' + wgUserLanguage);
	$j(document).ready(function() { StockPhoto.init () ; } ) ;
}

// i18n on subpages [[MediaWiki:StockPhoto.js/langcode]]:
// StockPhoto.i18n = { ... }
