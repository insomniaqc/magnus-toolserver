<?PHP

#@ob_start("ob_gzhandler");
error_reporting ( E_ALL ) ;
@set_time_limit ( 10*60 ) ; # Time limit 10min

$hide_header = true ;
$hide_doctype = true ;
include_once ( 'queryclass.php' ) ;

#_________________________________________________________________________________


function show_form () {
	global $title , $language , $project ;
  header('Content-type: text/html; charset=utf-8');
  print '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">' . "\n\n" ;
  print "<html><head></head><body>" ;
  print get_common_header ( "pdf.php" , "Wiki PDF creator" ) ;
  
  print "<form method='post'><table>
  <tr><th>Title(s)</th><td><input name='title' type='text' value='$title'/> (separate multiple titles with \"|\")</td></tr>
  <tr><th>Language</th><td><input name='language' type='text' value='$language'/></td></tr>
  <tr><th>Project</th><td><input name='project' type='text' value='$project'/></td></tr>
  <tr><th></th><td><input name='doit' type='submit' value='Do it'/></td></tr>
  </table></form>" ;
  
  print "</body></html>" ;
}

function delete_between ( &$html , $from , $to ) {
  $parts = explode ( $from , $html ) ;
  $html = array_shift ( $parts ) ;
  foreach ( $parts AS $p ) $html .= array_pop ( explode ( $to , $p , 2 ) ) ;
}

function get_html ( $title ) {
  global $language , $project , $server ;
  $begin = "<!-- start content -->" ;
  $end = "<!-- end content -->" ;
  $page_url = get_wikipedia_url ( $language , $title , "" , $project ) ;
  $html = file_get_contents ( $page_url . "&useskin=simple" ) ;
  $html = array_pop ( explode ( $begin , $html ) ) ;
  $html = array_shift ( explode ( $end , $html ) ) ;
  
  # Get rid of section edit links
  delete_between ( $html , '<span class="editsection">' , '</span>' ) ;
  
  # Get rid of TOC
  delete_between ( $html , '<table id="toc"' , '</table>' ) ;

  # Fixing local links
  $html = str_replace ('<a href="/', '<a href="'.$server.'/', $html);
  
  $html = trim ( $html ) ;
  $html = #"<!-- HEADER CENTER \"$page_url\" -->\n" .
  			"<h1>" . str_replace ( '_' , ' ' , $title ) . "</h1>\n" . 
			$html ;
  $html = utf8_decode ( $html ) ;
  return $html ;
}


#_________________________________________________________________________________

$title = get_request ( "title" , get_request ( "titles" , "" ) ) ;
$language = get_request ( "language" , "en" ) ;
$project = get_request ( "project" , "wikipedia" ) ;
$nogfdl = isset ( $_REQUEST['nogfdl'] ) ;

if ( $title == "" ) {
  show_form () ;
  exit ;
} else {

  $server = "http://$language.$project.org" ;
  $styles = array ( 
    "$server/skins-1.5/common/commonPrint.css?100",
    "$server/skins-1.5/common/shared.css?100",
    "$server/skins-1.5/simple/main.css?100",
    "$server/w/index.php?title=MediaWiki:Common.css&usemsgcache=yes&action=raw&ctype=text/css&smaxage=2678400",
    "$server/w/index.php?title=MediaWiki:Simple.css&usemsgcache=yes&action=raw&ctype=text/css&smaxage=2678400",
  ) ;
  
  $header = "" ;
  foreach ( $styles AS $s ) $header .= '<script type= "text/javascript" src="' . $s . "\"/>\n" ;


  $html = "" ;
  $titles = explode ( '|' , $title ) ;
  foreach ( $titles AS $t ) {
    $html .= get_html ( trim ( $t ) ) ;
  }

  $params = array () ;
  $page_title = "" ;
  if ( count ( $titles ) > 1 ) {
	$page_title = str_replace ( "_" , " " , $title ) ;
	$page_title = str_replace ( "|" , ", " , $page_title ) ;
    $outputtype = "--book" ;
    $params[] = "--toclevels 2" ;
  } else {
	$page_title = str_replace ( "_" , " " , $title ) ;
    $outputtype = "--book" ;
  }
  if ( $page_title != "" ) $header .= "<title>$page_title</title>" ;
  
  # Add page meta information
  $meta = "<h1>Source information</h1>" ;
  $meta .= "<table border='1'><tr><th>Page</th><th>Links</th></tr>" ;
  foreach ( $titles AS $t ) {
  	$meta .= "<tr>" ;
  	$meta .= "<th>" . str_replace ( "_" , " " , $t ) . "</th>" ;
  	$url = get_wikipedia_url ( $language , $t , "" , $project ) ;
  	$meta .= "<td>URL : <a href=\"$url\"><small>$url</small></a><br/>" ;
  	$url = get_wikipedia_url ( $language , $t , "history" , $project ) ;
  	$meta .= "Author list : <a href=\"$url\"><small>$url</small></a></td>" ;
  	$meta .= "</tr>" ;
  }
  $meta .= "</table>" ;
  $html .= utf8_decode ( $meta ) ;
  
  # Add GFDL
  if ( !$nogfdl ) {
    $gfdl = file_get_contents ( "./gfdl.txt" ) ;
    $html .= "<h1>GNU Free Documentation License (GFDL)</h1><font size='1'>$gfdl</font>" ;
  }
  
  # Generate
  $html = "<html><head>$header</head><body>$html</body></html>" ;
  $mytemp = "/tmp/f" .time(). "-" .rand() . ".html";
  $article_f = fopen($mytemp,'w');
  fwrite($article_f, $html);
  fclose($article_f);
  putenv("HTMLDOC_NOCGI=1");
#	header('Content-type: text/plain; charset=utf-8');
  header("Content-Type: application/pdf");
  header("Content-Disposition:attachment; filename=wiki.pdf");
  myflush();
  passthru("/home/magnus/bin/htmldoc -t pdf14 --charset iso-8859-1 --compression=9 --color " . implode ( " " , $params )  . "--quiet --jpeg $outputtype '$mytemp'");
  unlink ($mytemp);
}




?>