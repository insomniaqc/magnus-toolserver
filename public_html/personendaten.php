<?php

# Dieses Programm generiert Personendaten-Einträge für Personen auf de.wikipedia
# (c) 2004/2005 by Magnus Manske
# Lizensiert unter GPL

$may_not_contain = array (
	"[[Kategorie:Personengruppe" ,
	"[[Kategorie:Fiktive Person" ) ;

function parse_time_place ( $t , &$date , &$place )
	{
	$place = "" ;
	$date = "" ;
	$a = explode ( " in " , $t ) ;
	if ( count ( $a ) != 2 ) $a = explode ( " bei " , $t ) ;
	if ( count ( $a ) != 2 ) $a = explode ( " nahe " , $t ) ;
	if ( count ( $a ) == 2 )
		{
		$date = trim ( $a[0] ) ;
		$place = trim ( $a[1] ) ;
		}
	else $date = trim ( $t ) ;
	
	while ( $date != "" AND ( #substr ( $place , 0 , 1 ) != "[" OR
		  substr ( $date , 0 , 1 ) == "" OR
		  substr ( $date , 0 , 1 ) == "*" OR
		  substr ( $date , 0 , 1 ) == "?" OR
		  substr ( $date , 0 , 1 ) == "�" OR
		  substr ( $date , 0 , 1 ) == " " ) )
		  $date = trim ( substr ( $date , 1 ) ) ;
	}
	
function get_source ( $title )
	{
	$url = "http://de.wikipedia.org/w/wiki.phtml?title={$title}&action=raw" ;
	$handle = fopen ( $url , "r" ) ;
	$contents = '';
	while ( !feof ( $handle ) )
		$contents .= fread ( $handle , 81920 ) ;
	fclose ( $handle ) ;
	return $contents ;
	}

function rearrange ( $title )
	{
	global $may_not_contain ;
	$title = $ot = $title ;
	$title = str_replace ( " " , "_" , $title ) ;
	$in = get_source ( $title ) ;
#	$in = "'''Lucius Accius''' (* ca. [[170 v. Chr.]] bei [[Pesaro]]/[[Umbrien]]; � um [[90 v. Chr.]]) war ein römischer [[Tragödie|Tragödiendichter]]. Er entstammte einer Freigelassenenfamilie; sein genaues Todesjahr ist zwar unbekannt, er muss jedoch ein hohes Alter erreicht haben, da [[Cicero]] (106 - 43 v.Chr.) in \''Brutus\'' davon berichtet, sich mit ihm über Literatur unterhalten zu haben. {{Personendaten" ;
	
	$istagged = stristr ( $in , "{{Personendaten" ) ;
	if ( $istagged AND isset ( $_POST["ignoretagged"] ) AND $_POST["ignoretagged"] == 1 )
		{
		return "<font color='red'><b>\"{$title}\" enthält bereits Personendaten!</b></font><br>" ;
		}
		
	foreach ( $may_not_contain AS $mnk )
		{
		if ( stristr ( str_replace ( " " , "_" , $in ) , str_replace ( " " , "_" , $mnk ) ) )
			return "<font color='red'><b>\"{$title}\" enthält bösen Text \"{$mnk}\"!</b></font><br>" ;
		}
	
	$edit_url = "http://de.wikipedia.org/w/wiki.phtml?title={$title}&action=edit" ;
	$a = explode ( "\n" , $in ) ;
	$in = "" ;
	foreach ( $a AS $x )
		{
		if ( $in == "" AND count ( explode ( "'''" , $x ) ) > 1 )
			$in = $x ;
		}
	if ( $in == "" ) return "Problem mit <u>{$ot}</u>; <a href=\"{$edit_url}\">Bearbeiten</a>" ; # Something's wrong

	$before = explode ( "(" , $in , 2 ) ;
	while ( count ( $before ) < 2 ) $before[] = "" ;
	
	$after = explode ( ")" , $before[1] ) ;
	$desc = array () ;
	$cnt = 0 ;
	while ( $cnt >= 0 AND count ( $after ) > 0 )
		{
		$y = array_shift ( $after ) ;
		$desc[] = $y ;
		$cnt += count ( explode ( "(" , $y ) ) - 2 ;
		}
	$cur = implode ( ")" , $desc ) ;
	$desc = implode ( ")" , $after ) ;
	
	$name = array_shift ( $before ) ;
	
	# Fine-tuning name
	$name = trim ( str_replace ( "'" , "" , $name ) ) ;
	$n = explode ( " von " , $name , 2 ) ;
	if ( count ( $n ) < 2 ) $n = explode ( " " , $name ) ;
	$nn = trim ( array_pop ( $n ) ) ;
	$vn = trim ( implode ( " " , $n ) ) ;
	$name = $nn . ", " . $vn ;
	
	# Fine-tuning description
	$desc = trim ( $desc ) ;
	$d1 = strtolower ( substr ( $desc , 0 , 4 ) ) ;
	if ( $d1 == "war " OR $d1 == "ist " ) $desc = trim ( substr ( $desc , 4 ) ) ;
	$desc = trim ( $desc ) ;
	$d1 = strtolower ( substr ( $desc , 0 , 4 ) ) ;
	if ( $d1 == "ein " ) $desc = trim ( substr ( $desc , 4 ) ) ;
	$desc = explode ( ". " , $desc . " " ) ;
	$desc = ucfirst ( array_shift ( $desc ) ) ;

	# Fine-tuning birth/death
	$c = explode ( ";" , $cur , 2 ) ;
	if ( count ( $c ) == 1 )
		{
		$d = explode ( "," , $cur ) ;
		if ( count ( $d ) == 2 ) $c = $d ;
		else $c[] = "" ;
		}
	$birth = $c[0] ;
	$death = $c[1] ;
	parse_time_place ( $birth , $dob , $pob ) ;
	parse_time_place ( $death , $dod , $pod ) ;

	if ( $istagged ) $istagged = " <font color='red'><b>Dieser Artikel enthält bereits Personendaten!</b></font>" ;
	else $istagged = "" ;
	
	return "
<u>{$ot}</u> : <a href=\"{$edit_url}\">Bearbeiten</a>{$istagged}<br/>
<b>{$in}</b><br/>
<pre>
&lt;!-- Bitte nicht loeschen!
Zur Erklaerung siehe [[Wikipedia:Personendaten]]--&gt;

{{Personendaten| 
 NAME=<font color='red'>{$name}</font>
|ALTERNATIVNAMEN=
|KURZBESCHREIBUNG=<font color='red'>{$desc}</font>
|GEBURTSDATUM=<font color='red'>{$dob}</font>
|GEBURTSORT=<font color='red'>{$pob}</font>
|STERBEDATUM=<font color='red'>{$dod}</font>
|STERBEORT=<font color='red'>{$pod}</font>
}}
</pre>
" ;
	}

header( "Content-type: text/html; charset=utf-8" );

# Main program
print "<html><head></head><body>" ;

if ( isset ( $_POST["list"] ) || isset ( $_GET["name"] ) )
	{
	if ( isset ( $_GET["name"] ) )
		{
		$form = $_GET["name"] ;
		$a = explode ( ";" , $form ) ;
		}
	else
		{
		$form = $_POST["list"] ;
		$a = explode ( "\n" , $form ) ;
		}
	$y = array () ;
	foreach ( $a AS $x )
		{
		$x = trim ( $x ) ;
		while ( substr ( $x , 0 , 1 ) == '#' ) $x = trim ( substr ( $x , 1 ) ) ;
		if ( $x != "" )
			{
			$y[] = rearrange ( $x ) ;
			}
		}
	print implode ( "<hr>" , $y ) ;
	}

print "<h1>Artikelliste</h1>\n" ;
print "Zum automatischen Generieren von Personentags<br>" ;
print "Ein Artikelname pro Zeile<br>" ;
print "<form method=post><textarea name='list' style='width:100%' rows=20></textarea><br>" ;
print "<input type=submit value=\"Los geht's!\" default></input> <input CHECKED value=1 type=checkbox name=ignoretagged>Keine Personendaten für Artikel, die schon welche enthalten</input></form>" ;

print "</body></html>" ;

#print rearrange ( "Giuseppe Acerbi" ) ;

?>