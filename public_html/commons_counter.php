<?PHP

function req ( $key , $default = '' ) {
	if ( isset ( $_REQUEST[$key] ) ) return $_REQUEST[$key] ;
	return $default ;
}

function add_row_to_logfile ( $filename , $data , $force_new = false ) {
	ksort ( $data ) ;
	if ( !file_exists ( $filename ) ) $force_new = true ;
	$fp = '' ;
	if ( $force_new ) {
		$fp = fopen ( $filename , 'w' ) ;
		$a = array () ;
		foreach ( $data AS $k => $v ) $a[] = $k ;
		fwrite ( $fp , implode ( "\t" , $a ) . "\n" ) ;
		print "// Creating $filename\n" ;
	} else $fp = fopen ( $filename , 'a' ) ;
	
	$a = array () ;
	foreach ( $data AS $k => $v ) $a[] = $v ;
	fwrite ( $fp , implode ( "\t" , $a ) . "\n" ) ;
	
	fclose ( $fp ) ;
}

$path = '/mnt/user-store/mm6_logs' ;

header ( "Content-Type: text/JavaScript" ) ;

$type = req ( 'type' ) ;

if ( $type == 'extclick' ) {
	$keys = array ( 'page' , 'target' , 'server' , 'namespace' , 'userlang' , 'contlang' , 
		'articleid' , 'loggedin' , 'revision' , 'linktext' ) ;
	$data = array () ;
	foreach ( $keys AS $k ) {
		$s = req ( $k ) ;
		$data[$k] = str_replace ( "\t" , " " , $s ) ;
	}
	$data['timestamp'] = date ( 'YmdHis' )  ;
//	foreach ( $data AS $k => $v ) print "// $k : $v\n" ;
	$fn = "$path/extclick." . date ( 'Y-m' ) . ".tab" ;
	add_row_to_logfile ( $fn , $data ) ;
	print "location.href=\"" . $data['target'] . "\";" ;
}

?>
