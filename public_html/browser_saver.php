<?PHP
error_reporting ( E_ALL ) ;

include_once ( 'queryclass.php' ) ;

$category = get_request ( 'category' , '' ) ;
$language = get_request ( 'language' , 'commons' ) ;
$project = get_request ( 'project' , 'wikimedia' ) ;

?>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />


</head>
<body onload="init()" style="background:black;color:white;overflow:hidden" scroll="no">

<?PHP

if ( isset ( $_REQUEST['latest'] ) ) {
	print "<script type='text/javascript'>\n" ;
	print "var use_latest = true ;\n" ;
	print "files = new Array () ;\n" ;
	print "</script>" ;
} else if ( $category == '' ) {
	print "usage: ?category=CATEGORY_NAME" ;
} else {
	$files = db_get_images_in_category ( $language , $category , 0, $project ) ;
	print "<script type='text/javascript'>\n" ;
	print "var use_latest = false ;\n" ;
	print "files = new Array () ;\n" ;
	print "file_urls = new Array () ;\n" ;
	foreach ( $files AS $of ) {
		$f = str_replace ( '"' , '\"' , $of ) ;
		print "files.push ( \"$f\" ) ;\n" ;
		print "file_urls[\"$f\"] = \"" . 	get_image_url ( $language , $of , "wikipedia" ) . "\" ;\n" ;
	}
	print "</script>" ;
}

?>

<img id='image' width='1024px' style='display:none;position:absolute;left:200px;top:200px' />
<img id='dummy_image' width='1024px' style='display:none' />

</body></html>

<script type='text/javascript'>

function arrayShuffle(){
  var tmp, rand;
  for(var i =0; i < this.length; i++){
    rand = Math.floor(Math.random() * this.length);
    tmp = this[i]; 
    this[i] = this[rand]; 
    this[rand] =tmp;
  }
}

Array.prototype.shuffle =arrayShuffle;

current_file = 0 ;
animate = 1 ;
update_timeout = 50 ;
diff_x = -1 ;
diff_y = 1 ;

function update_pos () {
	var i = document.getElementById('image') ;
	var l = parseInt ( i.style.left ) ;
	var t = parseInt ( i.style.top ) ;

	var w = parseInt ( i.style.width ) ;
	var h = parseInt ( i.style.height ) ;
	
	var x = (l+diff_x) ;
	var y = (t+diff_y) ;
	
	if ( y <= 0 && diff_y < 0 ) diff_y = -diff_y ;
	if ( x <= 0 && diff_x < 0 ) diff_x = -diff_x ;
	
	i.style.left = x + "px" ;
	i.style.top = y + "px" ;
	
	setTimeout ( update_pos , update_timeout ) ;
}

function load_latest () {
	var x = new XMLHttpRequest();
}

function show_current_file () {
	if ( use_latest ) {
		load_latest () ;
		return ;
	}
	var f = files[current_file] ;
	var i = document.getElementById('image') ;
	i.style.display = 'none' ;
	i.src = file_urls[f] ;
//	i.style.top = "10px" ;
//	i.style.left = "10px" ;
	i.style.display = 'block' ;
	
	diff_x = -diff_x ;
	if ( diff_x > 0 ) diff_y = -diff_y ;

	
	current_file++ ;
	if ( current_file >= files.length ) current_file = 0 ;
	document.getElementById('dummy_image').src = file_urls[files[current_file]] ; // Pre-cache
	setTimeout ( show_current_file , 5000 ) ;
}

function init () {
	files.shuffle() ;
	show_current_file () ;
	setTimeout ( update_pos , update_timeout ) ;
}
</script>
