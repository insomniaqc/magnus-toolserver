<?PHP

$hide_header = true ;
$hide_doctype = true ;

set_include_path(get_include_path().PATH_SEPARATOR.'..');
include_once ( "queryclass.php") ;

function db_get_redirects_to ( $language , $project , $page , $ns = 0 , $source_ns = array ( 0 ) ) {
	$mysql_con = db_get_con_new($language,$project) ;
	$db = $language . 'wiki_p' ;
	$ret = array () ;
	$sns = implode ( ',' , $source_ns ) ;
	make_db_safe ( $sns ) ;
	make_db_safe ( $page ) ;
	make_db_safe ( $ns ) ;
	$sql = "SELECT DISTINCT page_title FROM page,redirect WHERE rd_namespace=$ns AND rd_title=\"$page\" AND page_id=rd_from AND page_namespace IN ( $sns )" ;

	$res = my_mysql_db_query ( $db , $sql , $mysql_con ) ;
	if ( mysql_errno() != 0 ) return $ret ;#{ print 'Line 171:' . mysql_error() . "<br/>" ; return $ret ; } # Some error has occurred
	while ( $o = mysql_fetch_object ( $res ) ) {
		$ret[$o->page_title] = $o->page_title ;
	}

	return $ret ;
}

$partners = array () ;
$partners['eol'] = array ( 'Encyclopedia of Life' , 'Hemigrapsus penicillatus by OpenCage.jpg' , '#AAFD8E' , 'EOL/Wikipedia book' ) ;
$partners['rfam'] = array ( 'Rfam' , 'RF00629.jpg' , '' , 'Rfam/Wikipedia book' ) ;
$partners['pfam'] = array ( 'Pfam' , 'Protein.png' , '' , 'Pfam/Wikipedia book' ) ;

$query = get_request ( 'query' , '' ) ;
$format = get_request ( 'format' , '' ) ;
$callback = get_request ( 'callback' , '' ) ;

$mysql_con_u = db_get_con() ;
if ( !isset ( $mysql_con_u ) ) { print "MySQL problem" ; exit ( 0 ) ; }

$out = array () ;

if ( $query == 'wikipage' ) {
	$page = get_request ( 'page' , '' ) ;
	$project = get_request ( 'project' , 'en.wikipedia' ) ;
	
	$p = explode ( '.' , $project ) ;
	$wp = array_pop ( $p ) ;
	$language = implode ( '.' , $p ) ;
	
	make_db_safe ( $page ) ;
	$pages = db_get_redirects_to ( $language , $wp , $page ) ;
	$pages[$page] = $page ;
	$pages = '"' . implode ( '","' , $pages ) . '"' ;
	
	$project = strtolower ( get_db_safe ( $project ) ) ;
	
	$sql = "SELECT * FROM sifted WHERE wikipage IN ($pages) AND wikiproject=\"$project\"" ;
	$res = @my_mysql_db_query ( "u_magnus_sifter_p" , $sql , $mysql_con_u ) ;
	if ( mysql_errno() != 0 ) print "BUG FIXME" ;
	while ( $o = mysql_fetch_object ( $res ) ) {
		$o->partner_name = $partners[$o->partner][0] ;
		$out[] = $o ;
	}
}

if ( $format == 'json' ) {
	header('Content-type: application/json; charset=utf-8');
	if ( $callback != '' ) print "$callback(" ;
	print json_encode ( $out ) ;
	if ( $callback != '' ) print ");" ;
} else if ( $format == 'jsonfm' ) {
	header('Content-type: text/plain; charset=utf-8');
	if ( $callback != '' ) print "$callback(" ;
	print json_encode ( $out ) ;
	if ( $callback != '' ) print ");" ;
} else if ( $format == 'phpfm' ) {
	header('Content-type: text/plain; charset=utf-8');
	if ( $callback != '' ) print "$callback(" ;
	print_r ( $out ) ;
	if ( $callback != '' ) print ");" ;
}

?>
