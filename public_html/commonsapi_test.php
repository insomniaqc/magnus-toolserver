<?PHP
/*
  Wikimedia Commons image API
  (c) 2008 by Magnus Manske
  Released under GPL
*/


// License detail data
$license_data = array (
  'GFDL' => array (
    'full_name' => 'GNU Free Documentation License',
    'attach_full_license_text' => '1',
    'attribute_author' => '1',
    'keep_under_license' => '1',
    'license_logo_url' => 'http://upload.wikimedia.org/wikipedia/commons/thumb/2/22/Heckert_GNU_white.svg/64px-Heckert_GNU_white.svg.png',
    'license_info_url' => 'http://www.gnu.org/copyleft/fdl.html',
    'license_text_url' => 'http://www.gnu.org/licenses/fdl.txt',
  ) ,
  'CC-BY-2.5' => array (
    'full_name' => 'Creative Commons Attribution V2.5',
    'attach_full_license_text' => '0',
    'attribute_author' => '1',
    'keep_under_license' => '0',
    'license_logo_url' => 'http://upload.wikimedia.org/wikipedia/commons/thumb/7/79/CC_some_rights_reserved.svg/90px-CC_some_rights_reserved.svg.png',
    'license_info_url' => 'http://creativecommons.org/licenses/by/2.5/',
    'license_text_url' => 'http://creativecommons.org/licenses/by/2.5/legalcode',
  ) ,
) ;

// Do not include these categories in the list
$ignore_categories = array (
  'Media with locations',
) ;




// Function to convert text in attributes and between tags to XML by changing some charactes to entities
function myurlencode ( $text ) {
	return str_replace ( array ( '&', '"', "'", '<', '>', "'" ), array ( '&amp;' , '&quot;', '&apos;' , '&lt;' , '&gt;', '&apos;' ), $text );
}


// Fake user agent
ini_set('user_agent','MSIE 4\.0b2;');

/* USEFUL TEST IMAGES
	ChathamHDY0016.JPG	location
	Sa-warthog.jpg      languages, featured
*/

// Get parameters
$testing = isset ( $_REQUEST['test'] ) ;
$img = $_REQUEST['image'] ;

// Die with explanation if no image given
if ( !isset ( $img ) ) {
	print "<h1>Wikimedia Commons API</h1>
<hr/>
Usage : commonsapi.php?image=IMAGENAME <br/>
IMAGENAME must not have an Image: prefix <br/>
Example : <a href='commonsapi.php?image=Sa-warthog.jpg'>Sa-warthog.jpg</a><br/>
<i>Note:</i> All attributes and texts are entity-encoded.
" ;
	exit ;
}

// Prepare and read rendered image page
$img = str_replace ( ' ' , '_' , $img ) ;
$url = "http://commons.wikimedia.org/wiki/Image:$img" ;
srand(time()) ;
$text = file_get_contents ( $url."?".rand() ) ;

// Info table
$matches = array () ;
preg_match_all ( '/span\s+id="field-[^"]+"/' , $text , &$matches ) ;
$matches = $matches[0] ;
$titles = array () ;
foreach ( $matches AS $m ) {
	$t = array_pop ( explode ( $m , $text , 2 ) ) ;
	$title = array () ;
	preg_match ( '/\s+title="[^"]*"/' , $t , &$title ) ;
	$title = $title[0] ;
	$title = array_pop ( explode ( 'title="' , $title , 2 ) ) ;
	$title = substr ( $title , 0 , -1 ) ;
	$title = urldecode ( $title ) ;
	
	$k = explode ( 'field-' , $m ) ;
	$k = str_replace ( '"' , '' , $k[1] ) ;
	$titles[$k] = $title ;
}

// Set default description
$desc = array () ;
if ( isset ( $titles['description'] ) ) $desc['default'] = $titles['description'] ;
else $desc['default'] = "" ;
unset ( $titles['description'] ) ;

// get file data via "normal" API
$ii_url = "http://commons.wikimedia.org/w/api.php?format=php&action=query&prop=imageinfo&iilimit=500&iiprop=timestamp|user|url|size|sha1|metadata&titles=Image:" . $img ;
$data = unserialize ( file_get_contents ( $ii_url ) ) ;
$data = array_shift ( $data['query']['pages'] ) ;
$data = $data['imageinfo'] ;
$file_url = $data[0]['url'] ;
if ( isset ( $data[0]['metadata'] ) ) $metadata = $data[0]['metadata'] ;
else $metadata = array () ;
$filedata = array () ;
foreach ( $data AS $k => $v ) {
  $fd = $v ;
  if ( isset ( $fd['metadata'] ) ) unset ( $fd['metadata'] ) ;
  if ( isset ( $fd['url'] ) ) unset ( $fd['url'] ) ;
  if ( isset ( $fd['comment'] ) ) unset ( $fd['comment'] ) ;
  $filedata[$k] = $fd ;
}
if ( !isset ( $filedata[0] ) ) $filedata[0] = array () ;
if ( !isset ( $filedata[0]['width'] ) ) $filedata[0]['width'] = 0 ;
if ( !isset ( $filedata[0]['height'] ) ) $filedata[0]['height'] = 0 ;
if ( !isset ( $filedata[0]['size'] ) ) $filedata[0]['size'] = 0 ;


// Detect descriptions in many languages
$language_names = array () ;
$matches = array () ;
preg_match_all ( '/div\s+class="description [a-z-]+"/' , $text , &$matches ) ;
$matches = $matches[0] ;
foreach ( $matches AS $m ) {
	$tx = explode ( $m , $text ) ;
	array_shift ( $tx ) ;

	$m = explode ( ' ' , $m ) ;
	$m = array_pop ( $m ) ;
	$m = substr ( $m , 0 , -1 ) ;

	$t = $tx[0] ;
	$t = explode ( '</span>' , $t , 2 ) ;
	
	$ln = array_shift ( $t ) ;
	$ln = array_pop ( explode ( '<b>' , $ln ) ) ;
	$ln = array_shift ( explode ( '</b>' , $ln ) ) ;
	$ln = str_replace ( ':' , '' , $ln ) ;
	$language_names[$m] = $ln ;
	
	$t = array_pop ( $t ) ;
	$tx = explode ( '</div>' , $t ) ;
	$t = "" ;
	while ( count ( $tx ) > 0 ) { // Hackish; works only if there's no <div> in the description. FIXME!
		$t2 = array_shift ( $tx ) ;
		$t .= $t2 ;
		break ;
	}
	
	if ( !isset ( $desc[$m] ) ) $desc[$m] = $t ;
	else $desc[$m] .= "<br/>\n$t" ;
}

// Categories
$self_made = '' ;
$cats = array () ;
preg_match_all ( '/ title="Category:[^"]+"\s*>/' , $text , &$matches ) ;
$matches = $matches[0] ;
foreach ( $matches AS $m ) {
	$m = array_pop ( explode ( ':' , $m , 2 ) ) ;
	$m = explode ( '"' , $m ) ;
	array_pop ( $m ) ;
	$m = implode ( '"' , $m ) ;
	if ( in_array ( $m , $ignore_categories ) ) continue ;
	if ( $m == 'Quality images' ) {
    $titles['qualityimage'] = 1 ; // Just to make sure...
    continue ;
	}
	if ( substr ( $m , 0 , 19 ) == 'Pictures of the day' ) {
    if ( !isset ( $titles['potd'] ) ) $titles['potd'] = trim ( substr ( $m , 21 , 4 ) ) . "0000" ;
    continue ;
	}
	if ( $m == 'Self-published work' ) {
    $self_made = ' selfmade="1"' ;
    continue ;
	}
	$cats[$m] = $m ;
}
ksort ( $cats ) ;

// Licenses (extracted from categories)
$licenses = array () ;
foreach ( $cats AS $cat ) {
	$c = strtolower ( $cat ) ;
	$lic = false ;
	if ( substr ( $c , 0 , 2 ) == 'pd' ) $lic = true ;
	else if ( substr ( $c , 0 , 4 ) == 'gfdl' ) $lic = true ;
	else if ( substr ( $c , 0 , 3 ) == 'cc-' ) $lic = true ;
	if ( $lic ) {
		$licenses[$cat] = $cat ;
		unset ( $cats[$cat] ) ;
	}
}


// Location
$location = '' ;
$matches = array () ;
preg_match_all ( '/<span\s+class="latitude">([0-9\.]+)<\/span>([^<]*)/' , $text , &$matches ) ;
if ( count ( $matches ) == 3 ) {
	$lat = $matches[1][0] . $matches[2][0] ;
	$matches = array () ;
	preg_match_all ( '/<span\s+class="longitude">([0-9\.]+)<\/span>([^<]*)/' , $text , &$matches ) ;
	if ( count ( $matches ) == 3 ) {
		$lon = $matches[1][0] . $matches[2][0] ;
		if ( $lat != '' && $lon != '' )
			$location = '<location><lat>' . $lat . '</lat><lon>' . $lon . '</lon></location>' ;
	}
}

// Fixes
$justbools = array ( 'featuredpicture' , 'qualityimage' ) ;
foreach ( $justbools AS $b ) {
	if ( isset ( $titles[$b] ) ) $titles[$b] = '1' ;
}

// Test mode output
if ( $testing ) {
	print "<pre>" ;
	print_r ( $data ) ;
/*	print_r ( $titles ) ;
	print_r ( $desc ) ;
	print_r ( $cats ) ;
	print_r ( $licenses ) ;*/
	print "</pre>" ;
	exit ;
}

// Now print this!
header('Content-type: text/xml; charset=utf-8');
print '<?xml version="1.0" encoding="UTF-8"?>' ;
print '<response version="0.9">' ;


print '<file>' ;
print '<name>' . myurlencode ( $img ) . '</name>' ;
print '<title>' . myurlencode ( "Image:$img" ) . '</title>' ;
print '<url>' . $file_url . '</url>' ;
print '<page>' . myurlencode ( $url ) . '</page>' ;
print '<size>' . myurlencode ( $filedata[0]['size'] ) . '</size>' ;
print '<width>' . myurlencode ( $filedata[0]['width'] ) . '</width>' ;
print '<height>' . myurlencode ( $filedata[0]['height'] ) . '</height>' ;
print $location ;
foreach ( $titles AS $k => $v ) {
  if ( $k == 'potd' ) {
    $k = 'pictureoftheday' ;
    $v = substr ( $v , 0 , 4 ) . '-' . substr ( $v , 4 , 2 ) . '-' . substr ( $v , 6 , 2 ) ;
  }
	print '<' . $k . '>' . myurlencode ( $v ) . '</' . $k . '>' ;
}
print '</file>' ;


if ( count ( $metadata ) > 0 ) {
  print '<meta>' ;
  foreach ( $metadata AS $k => $v ) {
    $k = strtolower ( $k ) ;
    print '<' . $k . '>' . myurlencode ( $v ) . '</' . $k . '>' ;
  }
  print '</meta>' ;
}


print '<description>' ;
foreach ( $desc AS $k => $v ) {
	print '<language code="' . $k . '"' ;
	if ( isset ( $language_names[$k] ) ) {
		print ' name="' . myurlencode ( $language_names[$k] ) . '"' ;
	}
	print '>' . myurlencode ( $v ) . '</language>' ;
}
print '</description>' ;


print '<categories>' ;
foreach ( $cats AS $v ) {
	print '<category>' . myurlencode ( $v ) . '</category>' ;
}
print '</categories>' ;


print '<licenses' . $self_made . '>' ;
foreach ( $licenses AS $l ) {
	print '<license>' ;
	print '<name>' . myurlencode ( $l ) . '</name>' ;
	if ( isset ( $license_data[$l] ) ) {
    foreach ( $license_data[$l] AS $k => $v ) {
      print '<' . $k . '>' . myurlencode ( $v ) . '</' . $k . '>' ;
    }
	}
	print '</license>' ;
}
print '</licenses>' ;


print '<versions>' ;
foreach ( $filedata AS $fd ) {
  print '<version>' ;
  foreach ( $fd AS $k => $v ) {
    print '<' . $k . '>' . myurlencode ( $v ) . '</' . $k . '>' ;
  }
  print '</version>' ;
}
print '</versions>' ;


print '</response>' ;

?>