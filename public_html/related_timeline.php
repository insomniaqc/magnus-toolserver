<?PHP

$hide_header = true ;
$hide_doctype = true ;

include_once ( 'common.php' ) ;

function set_min_max ( $val ) {
	global $first_year , $last_year ;
	if ( !isset ( $val ) ) return ;
	if ( $val < $first_year ) $first_year = $val ;
	if ( $val > $last_year ) $last_year = $val ;
}

function is_maintenance_cat ( $s ) {
	if ( preg_match ( '/^Articles_/' , $s ) ) return true ;
	if ( preg_match ( '/^Wikipedia_/' , $s ) ) return true ;
	if ( preg_match ( '/^Use_/' , $s ) ) return true ;
	if ( preg_match ( '/^BLP_/' , $s ) ) return true ;
	if ( preg_match ( '/^Orphaned_/' , $s ) ) return true ;
	if ( preg_match ( '/^Pages_/' , $s ) ) return true ;
	if ( preg_match ( '/^\d+_in_/' , $s ) ) return true ; // "_in_science", too generic
//	print "$s\n" ;
	return false ;
}


function get_number_cats ( $pattern , $m , $p ) {
	global $db , $mysql_con , $pid , $article ;
	$ret = array() ;
	
	// Links
	if ( $m == 0 or $m == 2 ) {
		$sql = "SELECT page_title,cl_to FROM page,categorylinks,pagelinks WHERE page_id=cl_from AND page_namespace=0 AND page_title=pl_title AND pl_namespace=0 AND pl_from=$pid AND cl_to $p \"$pattern\"" ;
		$res = my_mysql_db_query ( $db , $sql , $mysql_con ) ;
		if ( mysql_errno() != 0 ) exit ( 0 ) ;
		while ( $o = mysql_fetch_object ( $res ) ) {
			if ( is_maintenance_cat ( $o->cl_to ) ) continue ;
			$ret[$o->page_title][] = preg_replace ( '/\D/' , '' , $o->cl_to ) * 1 ;
		}
	}

	// Backlinks
	if ( $m == 1 or $m == 2 ) {
		$sql = "SELECT page_title,cl_to FROM page,categorylinks,pagelinks WHERE page_id=cl_from AND page_namespace=0 AND \"$article\"=pl_title AND pl_namespace=0 AND pl_from=page_id AND cl_to $p \"$pattern\"" ;
		$res = my_mysql_db_query ( $db , $sql , $mysql_con ) ;
		if ( mysql_errno() != 0 ) exit ( 0 ) ;
		while ( $o = mysql_fetch_object ( $res ) ) {
			if ( is_maintenance_cat ( $o->cl_to ) ) continue ;
			$ret[$o->page_title][] = preg_replace ( '/\D/' , '' , $o->cl_to ) * 1 ;
		}
	}
	
	return $ret ;
}

$callback = get_request ( 'callback' , '' ) ;
$article = get_request ( 'article' , '' ) ;
$language = get_request ( 'language' , 'en' ) ;
$project = get_request ( 'project' , 'wikipedia' ) ;
$format = get_request ( 'format' , 'json' ) ;

if ( $format == 'jsonfm' ) header('Content-type: text/plain; charset=utf-8');

if ( $article == '' ) {
	header('Content-type: text/plain; charset=utf-8');
	print "No article specified" ;
	exit ( 0 ) ;
}

$births = array ( 'en' => '%_births' , 'de' => 'Geboren_%' ) ;
$deaths = array ( 'en' => '%_deaths' , 'de' => 'Gestorben_%' ) ;


$db = get_db_name ( $language , $project ) ;
$mysql_con = db_get_con_new($language,$project) ;

make_db_safe ( $article ) ;
$sql = "SELECT page_id from page where page_namespace=0 and page_title=\"$article\"" ;
$res = my_mysql_db_query ( $db , $sql , $mysql_con ) ;
$pid = 0 ;
if ( mysql_errno() != 0 ) exit ( 0 ) ;
while ( $o = mysql_fetch_object ( $res ) ) {
	$pid = $o->page_id ;
}

$data = array() ;

// Births
if ( isset ( $births[$language] ) ) {
	$d2 = get_number_cats ( $births[$language] , 0 , 'LIKE' ) ;
	foreach ( $d2 AS $k => $v ) $data[$k]['birth'] = $v[0] ;
	$d2 = get_number_cats ( $births[$language] , 1 , 'LIKE' ) ;
	foreach ( $d2 AS $k => $v ) {
		if ( isset ( $data[$k]['birth'] ) ) $data[$k]['important'] = 1 ;
		else $data[$k]['birth'] = $v[0] ;
	}
}

// Deaths
if ( isset ( $deaths[$language] ) ) {
	$d2 = get_number_cats ( $deaths[$language] , 0 , 'LIKE' ) ;
	foreach ( $d2 AS $k => $v ) $data[$k]['death'] = $v[0] ;
	$d2 = get_number_cats ( $deaths[$language] , 1 , 'LIKE' ) ;
	foreach ( $d2 AS $k => $v ) $data[$k]['death'] = $v[0] ;
}

foreach ( $data AS $k => $v ) {
	$data[$k]['type'] = 'person';
}

// The rest
$d2 = get_number_cats ( "^[0-9]{3,4}\_.*" , 2 , 'REGEXP' ) ;
foreach ( $d2 AS $k => $v ) {
	if ( isset ( $data[$k] ) ) continue ;
	foreach ( $v AS $v2 ) $data[$k]['years'][$v2] = $v2 ;
	$data[$k]['type'] = 'unknown' ;
}

$d2 = get_number_cats ( "^.*\_[0-9]{3,4}$" , 2 , 'REGEXP' ) ;
foreach ( $d2 AS $k => $v ) {
	if ( isset ( $data[$k] ) and $data[$k]['type'] == 'person' ) continue ;
	if ( isset ( $data[$k] ) ) $data[$k]['important'] = '1' ;
	foreach ( $v AS $v2 ) $data[$k]['years'][$v2] = $v2 ;
	$data[$k]['type'] = 'unknown' ;
}

foreach ( $data AS $k => $v ) {
	if ( !isset ( $v['years'] ) ) continue ;
	$data[$k]['years'] = array_values ( $data[$k]['years'] ) ;
	asort ( $data[$k]['years'] ) ;
}

$first_year = 9999 ;
$last_year = 0 ;
foreach ( $data AS $k => $v ) {
	set_min_max ( $v['birth'] ) ;
	set_min_max ( $v['death'] ) ;
	if ( isset ( $v['years'] ) ) {
		foreach ( $v['years'] AS $y ) set_min_max ( $y ) ;
	}
}
$d = $data ;
$data = array ( 'first_year' => $first_year , 'last_year' => $last_year , 'pages' => $d ) ;

if ( $format == 'json' ) header('Content-type: application/json; charset=utf-8');

if ( $callback != '' ) print $callback.'(' ;
print json_encode ( $data  ) ;
if ( $callback != '' ) print ');' ;

?>
