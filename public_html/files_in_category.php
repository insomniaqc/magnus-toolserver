<?PHP
error_reporting ( E_ALL ) ;
$hide_header = true ;
$hide_doctype = true ;

include_once ( 'queryclass.php' ) ;




function start_html () {
	header('Content-type: text/html; charset=utf-8');
	print '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">' . "\n\n" ;
	print "<html>" ;
	print '<head><meta http-equiv="Content-Type" content="text/html; charset=utf-8" /></head>' ;
	print "<body>" ;
	print get_common_header ( "files_in_category.php" ) ;
}

function stop_html () {
	print "</body>" ;
	print "</html>" ;
}

function show_form () {
	global $language , $project , $category , $desc , $output ;
	
	$ocheck1 = $output == 'html' ? 'checked' : '' ;
	$ocheck2 = $output == 'xml' ? 'checked' : '' ;
	$ocheck3 = $output == 'text' ? 'checked' : '' ;

	$dcheck1 = $desc == 'none' ? 'checked' : '' ;
	$dcheck2 = $desc == 'wiki' ? 'checked' : '' ;
	$dcheck3 = $desc == 'html' ? 'checked' : '' ;
	
	print "<form method='post' action='files_in_category.php'><table border='1'>" ;
	print "<tr><th>Language</th><td><input type='text' name='language' value='$language' /> (or \"commons\")</td></tr>" ;
	print "<tr><th>Project</th><td><input type='text' name='project' value='$project' /></td></tr>" ;
	print "<tr><th>Category</th><td><input type='text' name='category' value='$category' /> (without \"Category:\" prefix!)</td></tr>" ;
	print "<tr><th>Description</th><td><input type='radio' name='desc' value='none' $dcheck1 />None | <input type='radio' name='desc' value='wiki' $dcheck2 />Wiki markup | <input type='radio' name='desc' value='html' $dcheck3 />HTML</td></tr>" ;
	print "<tr><th>Output</th><td><input type='radio' name='output' value='html' $ocheck1 />HTML | <input type='radio' name='output' value='xml' $ocheck2 />XML | <input type='radio' name='output' value='text' $ocheck3 />Text</td></tr>" ;
	print "<tr><th></th><td><input type='submit' name='doit' value='Do it!' /></td></tr>" ;
	print "</table></form>" ;
}


$language = fix_language_code ( get_request ( 'language' , 'en' ) ) ;
$project = check_project_name ( get_request ( 'project' , 'wikipedia' ) ) ;
$category = get_request ( 'category' ) ;
$desc = get_request ( 'desc' , 'none' ) ;
$output = get_request ( 'output' , 'html' ) ;
$doit = isset ( $_REQUEST['doit'] ) ;

if ( $language == 'commons' ) $project = 'wikimedia' ;

if ( $doit ) {
	if ( $output == 'xml' ) {
		header('Content-type: text/xml; charset=utf-8');
		print "<files>" ;
	} else if ( $output == 'text' ) {
		header('Content-type: text/plain; charset=utf-8');
	} else {
		start_html () ;
		show_form () ;
		print "<table border='1'>" ;
	}
	
	$files = db_get_images_in_category ( $language , $category , 0 , $project ) ;
	asort ( $files ) ;
	foreach ( $files AS $file ) {
		$nice_name = str_replace ( '_' , ' ' , $file ) ;
		$thumb_url = get_thumbnail_url ( $language , $file , 80 , $project ) ;

		$text = '' ;

		if ( $desc != 'none' ) {
			if ( $desc == 'wiki' ) {
				$text = file_get_contents ( "http://$language.$project.org/w/index.php?title=Image:$file&action=raw" ) ;
			} else if ( $desc == 'html' ) {
				$text = file_get_contents ( "http://$language.$project.org/wiki/Image:$file?action=render" ) ;
			}
		}

		if ( $output == 'xml' ) {
			print "<file>" ;
			print "<name>" . htmlspecialchars ( $nice_name ) . "</name>" ;
			if ( $desc == 'wiki' ) print "<wiki>" . htmlspecialchars ( $text ) . "</wiki>" ;
			if ( $desc == 'html' ) print "<html>" . htmlspecialchars ( $text ) . "</html>" ;
			print "</file>" ;
		} else if ( $output == 'text' ) {
			print "Image:$nice_name\n" ;
			if ( $text != '' ) print "$text\n\n" ;
			myflush() ;
		} else {
			print "<tr>" ;
			print "<th>$nice_name</th>" ;
			print "<td><a target='_blank' href='http://$language.$project.org/wiki/Image:$file'><img border='0' src='$thumb_url' /></a></td>" ;
			if ( $desc == 'wiki' ) print "<td style='font-size:8pt'><pre>$text</pre></td>" ;
			if ( $desc == 'html' ) print "<td style='font-size:8pt'>$text</td>" ;
			print "</tr>" ;
			myflush() ;
		}
	}
	
	
	if ( $output == 'xml' ) {
		print "</files>" ;
	} else if ( $output == 'text' ) {
	} else {
		print "</table>" ;
		stop_html () ;
		myflush() ;
	}
	
} else {
	start_html () ;
	show_form () ;
	stop_html () ;
}

?>
