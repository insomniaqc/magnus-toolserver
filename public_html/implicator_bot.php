#!/opt/ts/php/5.3/bin/php
<?PHP

// cat  people_with_parents.tab | ./bot.php

$testing = false ;

error_reporting ( E_ALL ) ;
ini_set("display_errors", 1);

require_once ( '/home/magnus/wikibase-api-php/include.php' ) ;

$user = "ImplicatorBot" ;
$password = trim ( file_get_contents ( "/home/magnus/ImplicatorBot/password" ) ) ;

const P_CHILD = 40 ; // page_id=3919593
const P_FATHER = 22 ; // page_id=3918088
const P_MOTHER = 25 ; // page_id=3918117
const P_SEX = 21 ;
const P_BROTHER = 7 ;
const P_SISTER = 9 ;
const P_ENTITY_TYPE = 107 ;

const Q_PERSON = 215627 ;
const Q_MALE = 6581097 ;
const Q_FEMALE = 6581072 ;

$claimPresenceImplications = array (
	array (
		"props" => array ( P_CHILD , P_FATHER , P_MOTHER , P_SEX ) ,
		"implies" => array ( P_ENTITY_TYPE => Q_PERSON )
	)
) ;

$directionalImplications = array (
	array (
		"q1_claim" => array ( P_CHILD ) , // q1 claims q2 is its child
		"q1_implications" => array ( P_ENTITY_TYPE => Q_PERSON ) , // q1 is a person
		"q2_implications" => array ( P_ENTITY_TYPE => Q_PERSON ) , // q2 is a person
	) ,
	array (
		"q1_claim" => array ( P_FATHER , P_BROTHER ) , // q1 claims q2 is father or brother
		"q1_implications" => array ( P_ENTITY_TYPE => Q_PERSON ) , // q1 is a person
		"q2_implications" => array ( P_ENTITY_TYPE => Q_PERSON , P_SEX => Q_MALE ) , // q2 is a person and male
	) ,
	array (
		"q1_claim" => array ( P_MOTHER , P_SISTER ) , // q1 claims q2 is father or brother
		"q1_implications" => array ( P_ENTITY_TYPE => Q_PERSON ) , // q1 is a person
		"q2_implications" => array ( P_ENTITY_TYPE => Q_PERSON , P_SEX => Q_FEMALE ) , // q2 is a person and female
	) ,
	array (
		"q1_claim" => array ( P_FATHER , P_MOTHER ) , // q1 claims q2 is father or mother
		"q2_implications" => array ( P_CHILD => 'q1' ) , // q2 has child q1
	)
) ;

function claimPresence ( $q ) {
	global $entities , $setItemClaims , $claimPresenceImplications ;
	foreach ( $claimPresenceImplications AS $dummy => $claim ) {
		$reasons = array() ;
		foreach ( $claim['props'] AS $v ) {
			$p = 'p'.$v ;
			$c = $entities[$q]->getClaimsForProperty ( $p ) ;
			if ( 0 < count ( $c ) ) $reasons[] = $p ;
		}
		if ( 0 == count ( $reasons ) ) continue ;
		foreach ( $claim['implies'] AS $k => $v ) {
	//		print "Presence of " . implode ( ',' , $reasons ) . " implies $k:$v\n" ;
			$setItemClaims[$q][$k] = $v ;
		}
	}
}

function addRelatedEntities ( $q ) {
	global $entities , $api ;
	foreach ( array ( P_FATHER , P_MOTHER , P_CHILD , P_BROTHER , P_SISTER ) AS $v ) {
		$p = 'p' . $v ;
		$claims = $entities[$q]->getClaimsForProperty ( $p ) ;
		if ( 0 == count ( $claims ) ) return ;
		foreach ( $claims AS $k => $c ) {
			$s = $c->getMainSnak() ; // TODO : All snarks
			$eid = $s->getDataValue() ;
			$q2 = 'q' . $eid->getNumericId() ;
			if ( isset ( $entities[$q2] ) ) continue ;
//			print "$p : $q2\n" ;
			$e2 = $api->getEntitiesFromIds( array( $q2 ) );
			$entities[$q2] = $e2[$q2] ;
		}
	}
}

function hasConnectingClaim ( $q1 , $q2 , $pnum ) {
	global $entities ;
	$ret = false ;
	$p = 'p' . $pnum ;
//	print "$q1 => $q2 via $p? " ;
	
	$claims = $entities[$q1]->getClaimsForProperty ( $p ) ;
	if ( 0 < count ( $claims ) ) {
		foreach ( $claims AS $k => $c ) {
			$s = $c->getMainSnak() ; // TODO : All snarks
			$eid = $s->getDataValue() ;
			$q = 'q' . $eid->getNumericId() ;
			if ( $q != $q2 ) continue ;
			$ret = true ;
			break ;
		}
	}
	
	
	return $ret ;
}

function setImplications ( $q , $q1 , $q2 , $implications ) {
	global $entities , $setItemClaims ;
	foreach ( $implications AS $k => $v ) {
		$x = $v ;
		if ( $v == 'q1' ) $x = substr ( $q1 , 1 ) ;
		if ( $v == 'q2' ) $x = substr ( $q2 , 2 ) ;
		$setItemClaims[$q][$k] = $x ;
	}
}

function claimRelations ( $q1 , $q2 ) {
	global $directionalImplications , $entities ;
	if ( $q1 == $q2 ) return ; // No selfing!
//	print "Checking relations $q1 => $q2\n" ;
	foreach ( $directionalImplications AS $di ) {
		$q1_ok = false ;
		foreach ( $di['q1_claim'] AS $pnum ) {
			if ( hasConnectingClaim ( $q1 , $q2 , $pnum ) ) $q1_ok = true ;
		}
		if ( !$q1_ok ) continue ;
		
		if ( isset ( $di['q1_implications'] ) ) setImplications ( $q1 , $q1 , $q2 , $di['q1_implications'] ) ;
		if ( isset ( $di['q2_implications'] ) ) setImplications ( $q2 , $q1 , $q2 , $di['q2_implications'] ) ;
		
	}
}

$cache = array() ;
$cache_file = '/home/magnus/ImplicatorBot/cache' ;
if ( file_exists ( $cache_file ) ) {
	$in = fopen($cache_file, 'r');
	while ( !feof($in)) {
		$q = strtolower ( trim ( fgets($in) ) ) ;
		if ( $q == '' ) break ;
		$cache[$q] = 1 ;
	}
	fclose($in);
//	$t = file_get_contents ( $cache_file ) ;
//	$cache = explode ( "\n" , $t ) ;
}

//print_r ( $cache ) ; exit ( 0 ) ;

//Instance the Api object with the base domain of the wiki and the user agent.
$api = new WikibaseApi( 'www.wikidata.org', 'ImplicatorBot/1.0' );

//login
$api->login( $user , $password );

$out = fopen($cache_file, 'w');
$handle = fopen('php://stdin', 'r');
while ( !feof($handle)) {

	$q = strtolower ( trim ( fgets($handle) ) ) ;
	if ( $q == '' ) break ;
	if ( $q == 'page_title' ) continue ;
//	if ( in_array ( $q , $cache ) ) {
	if ( isset ( $cache[$q] ) ) {
		fwrite ( $out , "$q\n" ) ;
//		print "SKIPPING $q\n" ;
		continue ; // Did this last time
	}
	$q_orig = $q ;
	print "CHECKING $q\n" ;

	$setItemClaims = array () ;

	//Get some entities
	$entities = $api->getEntitiesFromIds( array( $q ) );
	addRelatedEntities ( $q ) ;
	claimPresence ( $q ) ;

	//print implode ( ", " , array_keys ( $entities ) ) . "\n" ;

	foreach ( $entities AS $q1 => $v1 ) {
		foreach ( $entities AS $q2 => $v2 ) {
			claimRelations ( $q1 , $q2 ) ;
		}
	}


//	print "\nSETTING CLAIMS\n" ;
	foreach ( $setItemClaims AS $q => $qv ) {
		if ( !isset ( $entities[$q] ) ) return ;
		foreach ( $qv AS $k => $v ) {
			$p = 'p' . $k ;
			$c = $entities[$q]->getClaimsForProperty ( $p ) ;
			if ( 0 < count ( $c ) ) {
//				print "$q already has property $p set, skipping.\n" ;
				continue ;
			}
			$q2 = 'q' . $v ;
//			print "$q : $p => $q2\n" ;
			if ( $testing ) continue ;
			$statement = $entities[$q]->createStatementForSnak( new Snak( 'value', $p, EntityId::newFromPrefixedId( $q2 ) ) );
			$statement->save( "Setting implicit property $p to $q2" );
		}
	}
	
	fwrite ( $out , "$q_orig\n" ) ;
	
}
fclose ( $out ) ;

print "ALL DONE\n" ;

?>