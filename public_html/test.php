<?PHP

include "common.php" ;

// THIS WORKS!!
function get_category_tree_sql ( $rootcat , $maxdepth , $ns ) {

	if ( is_array ( $ns ) and count ( $ns ) == 1 ) $ns = array_shift ( $ns ) ;
	$sql = '' ;
	for ( $depth = 1 ; $depth <= $maxdepth ; $depth++ ) {
	
		if ( $sql != '' ) $sql .= " UNION " ;
	
		$sql .= "SELECT p1.page_title AS title,p1.page_id AS id,p1.page_namespace AS namespace FROM " ; // count(*) AS cnt,
		
		for ( $i = 1 ; $i <= $depth ; $i++ ) {
			if ( $i > 1 ) $sql .= "," ;
			$sql .= "page p{$i},categorylinks c{$i}" ;
		}
		
		$sql .= " WHERE " ;
		
		$cond = array() ;
		for ( $i = 1 ; $i <= $depth ; $i++ ) {
			if ( $i == 1 ) {
				if ( !isset ( $ns ) ) {
				} else if ( is_array ( $ns ) ) {
					$cond[] = "p{$i}.page_namespace IN (" . join ( ',' , $ns ) . ")" ;
				} else {
					$cond[] = "p{$i}.page_namespace={$ns}" ;
				}
				
			} else $cond[] = "p{$i}.page_namespace=14" ;
			
			$cond[] = "p{$i}.page_id=c{$i}.cl_from" ;
			
			if ( $i < $depth ) {
				$j = $i + 1 ;
				$cond[] = "c{$i}.cl_to=p{$j}.page_title" ;
			} else {
				$cond[] = sprintf ( "c%s.cl_to='%s'" , $i , $rootcat ) ;
			}
		
			if ( $i > 1 ) $cond[] = "c{$i}.cl_type='subcat'" ;
		}
		
		$sql .= join ( ' AND ' , $cond ) ;
	}
	
	$sql .= " ORDER BY title" ;
	return $sql ;
}

//$sql = get_category_tree_sql ( 'Scientific_instruments' , 5 , null ) ;
$sql = get_category_tree_sql ( 'German_scientists' , 5 , null ) ;

print "<div style='max-width:1000px'><tt>$sql</tt></div>" ;

?>